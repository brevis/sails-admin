var _ = require('lodash');
var _super = require('sails-auth/api/controllers/UserController');

_.merge(exports, _super);
_.merge(exports, {

    // Extend with custom logic here by adding additional fields, methods, etc.

    create: function (req, res) {
        return sails.models.user.register({
            username: req.body.username,
            password: req.body.password,
            email: req.body.email,
            roles: req.body.roles,
            createdBy: req.user.id,
            owner: req.user.id,
        }).then(function (user) {
            // TODO: add permissions
            res.send(user);
        });
    },

    getRoles: function (req, res) {
        User.findOne({id: req.param('id')}).populate('roles').exec(function (err, user) {
            var roles = [];
            // TODO: research how to short code
            if (user && user.id) {
                for (var k in user.roles) {
                    var role = user.roles[k];
                    if (role && role.id) {
                        roles.push(role.name);
                    }
                }
            }
            res.json(roles);
        });
    },

    getPermissions: function (req, res) {
        User.findOne({id: req.param('id')}).populate('permissions').exec(function (err, user) {
            var permissions = [];
            if (user && user.id) {
                for (var k in user.permissions) {
                    var permission = user.permissions[k];
                    if (role && role.id) {
                        permissions.push(permission.name);
                    }
                }
            }
            res.json(permissions);
        });
    },

    getDetails: function (req, res) {
        User.findOne({id: req.param('id')})
            .populate('roles')
            .populate('permissions')
            .populate('group').exec(function (err, user) {
            res.json(user);
        });
    }

});