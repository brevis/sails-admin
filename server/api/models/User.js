var _ = require('lodash');
var _super = require('sails-permissions/api/models/User');

_.merge(exports, _super);
_.merge(exports, {

    // Extend with custom logic here by adding additional fields, methods, etc.

    attributes: {
        group: {
            model: 'group'
        },
        widgets: {
            model: 'widget',
            via: 'users'
        },
    },

    afterUpdate: function(updatedUser, cb) {

        // TODO: validate password;

        if (updatedUser.password) {
            Passport.findOne({user: updatedUser.id, protocol: "local"}).then(function(passport) {
                if (passport) {
                    passport.password = updatedUser.password;
                    passport.save();

                    User.update({id: updatedUser.id}, {password: ''}).exec(function(err, user){
                        if (user) {
                            cb();
                        } else {
                            cb(false);
                        }
                    });
                } else {
                    cb(false);
                }
            });
        } else {
            cb();
        }
    }


});