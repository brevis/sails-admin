var _ = require('lodash');
var _super = require('sails-permissions/api/models/Role');

_.merge(exports, _super);
_.merge(exports, {

    // Extend with custom logic here by adding additional fields, methods, etc.
    ROLE_PUBLIC: 'public',
    ROLE_REGISTERED: 'registered',
    ROLE_ADMIN: 'admin',

    attributes: {
        reserved: {
            type: 'boolean'
        }
    }

});