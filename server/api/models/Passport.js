var _ = require('lodash');
var _super = require('sails-permissions/api/models/Passport');

_.merge(exports, _super);
_.merge(exports, {

    autoCreatedBy: true,

    attributes: {
        password: {type: 'string', minLength: 6},
    }

});