(function (global) {
    System.config({


        transpiler: 'ts',
        typescriptOptions: {
            // Complete copy of compiler options in standard tsconfig.json
            "target": "es5",
            "module": "commonjs",
            "moduleResolution": "node",
            "sourceMap": true,
            "emitDecoratorMetadata": true,
            "experimentalDecorators": true,
            "lib": ["es2015", "dom"],
            "noImplicitAny": true,
            "suppressImplicitAnyIndexErrors": true,
        },
        meta: {
            'typescript': {
                "exports": "ts"
            }
        },


        paths: {
            // paths serve as alias
            //'npm:': 'https://unpkg.com/'
            'npm:': '/js/node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: '/js/app',

            // angular bundles
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/material': 'npm:@angular/material/bundles/material.umd.js',
            '@angular/animations': 'npm:@angular/animations/bundles/animations.umd.js',
            '@angular/animations/browser': 'npm:@angular/animations/bundles/animations-browser.umd.js',
            '@angular/platform-browser/animations': 'npm:@angular/platform-browser/bundles/platform-browser-animations.umd.js',

            // other libraries
            'rxjs': 'npm:rxjs',
            'ts': 'npm:plugin-typescript/lib/plugin.js',
            'typescript': 'npm:typescript/lib/typescript.js',
            'es6-promise': 'npm:es6-promise/dist/es6-promise.min.js',
            'ng2-dnd': 'npm:ng2-dnd/bundles/index.umd.js',

        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            app: {
                main: 'main.js',
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            }
        }
    });

})(this);


/*
(function(global) {

    // map tells the System loader where to look for things
    var map = {
        'app':                        '/js/app',
        '@angular':                   '/js/node_modules/@angular',
        'rxjs':                       '/js/node_modules/rxjs',
        'ts': '/js/node_modules/plugin-typescript/lib/plugin.js',
        'typescript': '/js/node_modules/typescript/lib/typescript.js',
        'es6-promise': '/js/node_modules/es6-promise/dist/es6-promise.min.js',
        'ng2-dnd': '/js/node_modules/ng2-dnd/bundles/index.umd.js',
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app':                        { main: 'main.ts',  defaultExtension: 'ts' },
        'rxjs':                       { defaultExtension: 'js' }
    };

    var ngPackageNames = [
        'common',
        'compiler',
        'core',
        'forms',
        'http',
        'platform-browser',
        'platform-browser-dynamic',
        'router',
        'animations',
        'animations/browser',
        'material',
    ];

    // Individual files (~300 requests):
    function packIndex(pkgName) {
        packages['@angular/'+pkgName] = { main: 'index.js', defaultExtension: 'js' };
    }

    // Bundled (~40 requests):
    function packUmd(pkgName) {
        packages['@angular/'+pkgName] = { main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
    }

    // Most environments should use UMD; some (Karma) need the individual index files
    var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;

    // Add package entries for angular packages
    ngPackageNames.forEach(setPackageConfig);

    var config = {
        transpiler: 'ts',
        typescriptOptions: {
        // Complete copy of compiler options in standard tsconfig.json
        "target": "es5",
            "module": "commonjs",
            "moduleResolution": "node",
            "sourceMap": true,
            "emitDecoratorMetadata": true,
            "experimentalDecorators": true,
            "lib": ["es2015", "dom"],
            "noImplicitAny": true,
            "suppressImplicitAnyIndexErrors": true,
    },
    meta: {
        'typescript': {
            "exports": "ts"
        }
    },
        map: map,
        packages: packages
    };

    System.config(config);

})(this);
*/