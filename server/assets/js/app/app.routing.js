System.register("app.routing", ['@angular/router', './home/index', './widgets/index', './login/index', './logout/index', './admin/dashboard/index', './admin/users/index', './admin/user-edit/index', './admin/roles/index', './admin/role-edit/index', './admin/groups/index', './admin/group-edit/index', './admin/permissions/index', './admin/permission-edit/index', './admin/widgets/index', './admin/widget-edit/index', './admin/widgets-positions/index', './_guards/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, index_1, index_2, index_3, index_4, index_5, index_6, index_7, index_8, index_9, index_10, index_11, index_12, index_13, index_14, index_15, index_16, index_17;
    var appRoutes, routing;
    return {
        setters:[
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (index_2_1) {
                index_2 = index_2_1;
            },
            function (index_3_1) {
                index_3 = index_3_1;
            },
            function (index_4_1) {
                index_4 = index_4_1;
            },
            function (index_5_1) {
                index_5 = index_5_1;
            },
            function (index_6_1) {
                index_6 = index_6_1;
            },
            function (index_7_1) {
                index_7 = index_7_1;
            },
            function (index_8_1) {
                index_8 = index_8_1;
            },
            function (index_9_1) {
                index_9 = index_9_1;
            },
            function (index_10_1) {
                index_10 = index_10_1;
            },
            function (index_11_1) {
                index_11 = index_11_1;
            },
            function (index_12_1) {
                index_12 = index_12_1;
            },
            function (index_13_1) {
                index_13 = index_13_1;
            },
            function (index_14_1) {
                index_14 = index_14_1;
            },
            function (index_15_1) {
                index_15 = index_15_1;
            },
            function (index_16_1) {
                index_16 = index_16_1;
            },
            function (index_17_1) {
                index_17 = index_17_1;
            }],
        execute: function() {
            appRoutes = [
                // front
                { path: '', component: index_1.HomeComponent },
                { path: 'widgets', component: index_2.WidgetsComponent, canActivate: [index_17.RegisteredAuthGuard] },
                // admin panel
                { path: 'admin', component: index_5.AdminDashboardComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/dashboard', component: index_5.AdminDashboardComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/users', component: index_6.AdminUsersComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/user/edit/:id', component: index_7.AdminUserEditComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/groups', component: index_10.AdminGroupsComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/group/edit/:id', component: index_11.AdminGroupEditComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/roles', component: index_8.AdminRolesComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/role/edit/:id', component: index_9.AdminRoleEditComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/permissions', component: index_12.AdminPermissionsComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/permission/edit/:id', component: index_13.AdminPermissionEditComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/widgets', component: index_14.AdminWidgetsComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/widget/edit/:id', component: index_15.AdminWidgetEditComponent, canActivate: [index_17.AdminAuthGuard] },
                { path: 'admin/widgets/positions', component: index_16.AdminWidgetsPositionsComponent, canActivate: [index_17.AdminAuthGuard] },
                // auth
                { path: 'auth/login', name: 'login', component: index_3.LoginComponent },
                { path: 'auth/logout', name: 'logout', component: index_4.LogoutComponent },
                // otherwise redirect to home
                { path: '**', redirectTo: '' }
            ];
            exports_1("routing", routing = router_1.RouterModule.forRoot(appRoutes));
        }
    }
});
