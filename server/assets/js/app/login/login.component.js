System.register("login.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent(route, router, authenticationService, alertService, http) {
                    this.route = route;
                    this.router = router;
                    this.authenticationService = authenticationService;
                    this.alertService = alertService;
                    this.http = http;
                    this.model = {};
                    this.loading = false;
                }
                LoginComponent.prototype.ngOnInit = function () {
                    this.authenticationService.logout();
                    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
                    var currentUser = this.authenticationService.getCurrentUser();
                    if (currentUser.id) {
                        this.router.navigate([this.returnUrl]);
                    }
                };
                LoginComponent.prototype.login = function () {
                    var _this = this;
                    this.loading = true;
                    this.authenticationService.login(this.model.username, this.model.password)
                        .subscribe(function (data) {
                        _this.router.navigate([_this.returnUrl]);
                        _this.loading = false;
                    }, function (error) {
                        var errorMsg = error;
                        if (error.status == 403) {
                            errorMsg = 'Username or Password wrong';
                        }
                        _this.alertService.error(errorMsg);
                        _this.loading = false;
                    });
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'login.component.html',
                        styleUrls: ['login.component.css']
                    })
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
