System.register("admin-groups.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var AdminGroupsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AdminGroupsComponent = (function () {
                function AdminGroupsComponent(dataService, alertService) {
                    this.dataService = dataService;
                    this.alertService = alertService;
                    this.currentGroup = null;
                    this.loadGroups();
                }
                AdminGroupsComponent.prototype.deleteGroup = function (group) {
                    var _this = this;
                    var groupId = group.id;
                    this.dataService.destroy(group.id).subscribe(function (_group) {
                        if (_group && groupId === _group.id) {
                            _this.loadGroups();
                        }
                        else {
                            _this.alertService.error('Group with id "' + groupId + '" not found');
                        }
                        _this.currentGroup = null;
                    });
                };
                AdminGroupsComponent.prototype.loadGroups = function () {
                    var _this = this;
                    this.dataService.getAll().subscribe(function (groups) { _this.groups = groups; });
                };
                AdminGroupsComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-groups.component.html'
                    })
                ], AdminGroupsComponent);
                return AdminGroupsComponent;
            }());
            exports_1("AdminGroupsComponent", AdminGroupsComponent);
        }
    }
});
