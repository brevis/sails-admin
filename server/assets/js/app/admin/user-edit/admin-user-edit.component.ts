import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { User, Role, Group } from '../../_models/index';
import { UsersDataService, GroupsDataService, RolesDataService } from '../../_services/index';
import { CustomValidators } from '../../_validators/validators';
import { MdButtonModule, MdInputModule, MdSelectModule, MdGridListModule } from '@angular/material';


@Component({
    moduleId: module.id,
    templateUrl: 'admin-user-edit.component.html',
    styleUrls: ['admin-user-edit.component.css'],
})

export class AdminUserEditComponent implements OnInit {

    protected userId: string;

    protected user: User = null;

    protected userRolesIds: string[] = [];

    protected userPermissionsIds: string[] = [];

    protected groups: Group[] = [];

    protected roles: Role[] = [];

    protected userForm: FormGroup;

    protected formErrors = {
        username: '',
        email: '',
        password: '',
        roles: '',
        group: '',
    };

    protected validationMessages = {
        username: {
            required:      'Required.',
            minlength:     'Must be at least 3 characters long.',
            maxlength:     'Cannot be more than 30 characters long.',
            uniqueUserUsername: 'This Username already taken.',
        },
        email: {
            required: 'Required.',
            email: 'Incorrect.',
            uniqueUserEmail: 'This Email already taken.',
        },
        password: {
            minlength: 'Must be at least 6 characters long.',
        },
        roles: {
            required: 'Required.',
        },
    };

    constructor(
        private usersDataService: UsersDataService,
        private rolesDataService: RolesDataService,
        private groupsDataService: GroupsDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private formBuilder: FormBuilder) {

    }


    ngOnInit() {
        this.loadUser();
        this.rolesDataService.getAll().subscribe(roles => {
            this.roles = roles;
        });
        this.groupsDataService.getAll().subscribe(groups => {
            this.groups = groups;
        });
    }

    loadUser() {
        this.userId = this.route.snapshot.params.id;
        if (this.userId === '0') {
            this.user = new User();
            this.buildForm();
        } else {
            this.usersDataService.getUserDetails(this.userId).subscribe(user => {
                this.user = user;

                user.roles.forEach(role => {
                    this.userRolesIds.push(role.id);
                });

                user.permissions.forEach(permission => {
                    this.userPermissionsIds.push(permission.id);
                });

                this.buildForm();
            }, err => {
                this.alertService.error(`User with id "${this.userId}" not found.`);
            });
        }
    }

    buildForm() {

        let passwordsValidators = [Validators.minLength(6)];
        if (this.user.id === '0') {
            passwordsValidators.push(Validators.required);
        }

        this.userForm = this.formBuilder.group({
            username: new FormControl(this.user.username, [Validators.required, Validators.minLength(3), Validators.maxLength(30), CustomValidators.uniqueUserUsername(this.user.id)]),
            email: new FormControl(this.user.email, [Validators.required, Validators.email, CustomValidators.uniqueUserEmail(this.user.id)]),
            password: new FormControl('', passwordsValidators),
            roles: new FormControl(this.userRolesIds, [Validators.required]),
            group: new FormControl(this.user.group),
        });

        this.userForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.userForm) {
            return;
        }
        const form = this.userForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    saveUser() {
        const formData = this.userForm.value;
        if (this.userId === '0') {
            this.usersDataService.add(formData).subscribe(user => {
                this.alertService.success('User was added');
                this.user = new User();
                this.buildForm();
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        } else {
            this.usersDataService.update(this.userId, formData).subscribe(user => {
                this.alertService.success('User was updated');
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        }
    }

    cancel() {
        this.router.navigate(['/admin/users']);
    }


}
