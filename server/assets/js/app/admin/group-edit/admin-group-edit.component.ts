import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { Group } from '../../_models/group';
import { GroupsDataService } from '../../_services/groups-data.service';

@Component({
    moduleId: module.id,
    templateUrl: 'admin-group-edit.component.html'
})

export class AdminGroupEditComponent implements OnInit {

    protected groupId: string;
    protected group: Group = null;
    protected groupForm: FormGroup = new FormGroup({
        name: new FormControl(''),
    });

    protected formErrors = {
        name: '',
    };

    protected validationMessages = {
        name: {
            required: 'Required.',
            maxlength: 'Cannot be more than 30 characters long.',
        },
    };

    constructor(
        private dataService: GroupsDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.loadGroup();
    }

    loadGroup() {
        this.groupId = this.route.snapshot.params.id;
        if (this.groupId === '0') {
            this.group = new Group();
            this.buildForm();
        } else {
            this.dataService.getOne(this.groupId).subscribe(group => {
                this.group = group;
                this.buildForm();
            }, err => {
                this.alertService.error(`Group with id "${this.groupId}" not found.`);
            });
        }
    }

    buildForm() {
        this.groupForm = new FormGroup({
            name: new FormControl(this.group.name, [Validators.required, Validators.maxLength(30)]),
        });

        this.groupForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.groupForm) {
            return;
        }
        const form = this.groupForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    saveGroup() {
        const formData = this.groupForm.value;
        if (this.groupId === '0') {
            this.dataService.add(formData).subscribe(group => {
                this.alertService.success('Group was added');
                this.group = new Group();
                this.buildForm();
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        } else {
            this.dataService.update(this.groupId, formData).subscribe(group => {
                this.alertService.success('Group was updated');
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        }
    }

    cancel() {
        this.router.navigate(['/admin/groups']);
    }

}
