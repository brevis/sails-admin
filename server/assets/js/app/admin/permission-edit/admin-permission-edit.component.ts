import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { Permission } from '../../_models/permission';
import { PermissionsDataService } from '../../_services/permissions-data.service';
import { CustomValidators } from '../../_validators/validators';

@Component({
    moduleId: module.id,
    templateUrl: 'admin-permission-edit.component.html'
})

export class AdminPermissionEditComponent implements OnInit {

    protected permissionId: string;
    protected permission: Permission = null;
    protected permissionForm: FormGroup = new FormGroup({
        name: new FormControl(''),
        description: new FormControl(''),
    });

    protected formErrors = {
        name: '',
        description: '',
    };

    protected validationMessages = {
        name: {
            required:      'Required.',
            maxlength:     'Cannot be more than 30 characters long.',
        },
        description: {
            maxlength:     'Cannot be more than 100 characters long.',
        },
    };

    constructor(
        private dataService: PermissionsDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.loadPermission();
    }

    loadPermission() {
        this.permissionId = this.route.snapshot.params.id;
        if (this.permissionId === '0') {
            this.permission = new Permission();
            this.buildForm();
        } else {
            this.dataService.getOne(this.permissionId).subscribe(permission => {
                this.permission = permission;
                this.buildForm();
            }, err => {
                this.alertService.error(`Permission with id "${this.permissionId}" not found.`);
            });
        }
    }

    buildForm() {
        this.permissionForm = new FormGroup({
            name: new FormControl(this.permission.name, [Validators.required, Validators.maxLength(30), CustomValidators.uniquePermissionName(this.permission.id)]),
            description: new FormControl(this.permission.description, [Validators.maxLength(100)]),
        });

        this.permissionForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.permissionForm) {
            return;
        }
        const form = this.permissionForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    savePermission() {
        const formData = this.permissionForm.value;
        if (this.permissionId === '0') {
            this.dataService.add(formData).subscribe(permission => {
                this.alertService.success('Permission was added');
                this.permission = new Permission();
                this.buildForm();
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        } else {
            this.dataService.update(this.permissionId, formData).subscribe(permission => {
                this.alertService.success('Permission was updated');
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        }
    }

    cancel() {
        this.router.navigate(['/admin/permissions']);
    }

}
