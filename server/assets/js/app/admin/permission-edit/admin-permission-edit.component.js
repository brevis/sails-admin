System.register("admin-permission-edit.component", ['@angular/core', '@angular/forms', '../../_models/permission', '../../_validators/validators'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, forms_1, permission_1, validators_1;
    var AdminPermissionEditComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (permission_1_1) {
                permission_1 = permission_1_1;
            },
            function (validators_1_1) {
                validators_1 = validators_1_1;
            }],
        execute: function() {
            AdminPermissionEditComponent = (function () {
                function AdminPermissionEditComponent(dataService, location, router, route, alertService) {
                    this.dataService = dataService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.permission = null;
                    this.permissionForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl(''),
                        description: new forms_1.FormControl('')
                    });
                    this.formErrors = {
                        name: '',
                        description: ''
                    };
                    this.validationMessages = {
                        name: {
                            required: 'Required.',
                            maxlength: 'Cannot be more than 30 characters long.'
                        },
                        description: {
                            maxlength: 'Cannot be more than 100 characters long.'
                        }
                    };
                }
                AdminPermissionEditComponent.prototype.ngOnInit = function () {
                    this.loadPermission();
                };
                AdminPermissionEditComponent.prototype.loadPermission = function () {
                    var _this = this;
                    this.permissionId = this.route.snapshot.params.id;
                    if (this.permissionId === '0') {
                        this.permission = new permission_1.Permission();
                        this.buildForm();
                    }
                    else {
                        this.dataService.getOne(this.permissionId).subscribe(function (permission) {
                            _this.permission = permission;
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error("Permission with id \"" + _this.permissionId + "\" not found.");
                        });
                    }
                };
                AdminPermissionEditComponent.prototype.buildForm = function () {
                    var _this = this;
                    this.permissionForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl(this.permission.name, [forms_1.Validators.required, forms_1.Validators.maxLength(30), validators_1.CustomValidators.uniquePermissionName(this.permission.id)]),
                        description: new forms_1.FormControl(this.permission.description, [forms_1.Validators.maxLength(100)])
                    });
                    this.permissionForm.valueChanges
                        .subscribe(function (data) { return _this.onValueChanged(data); });
                    this.onValueChanged();
                };
                AdminPermissionEditComponent.prototype.onValueChanged = function (data) {
                    if (!this.permissionForm) {
                        return;
                    }
                    var form = this.permissionForm;
                    for (var field in this.formErrors) {
                        // clear previous error message (if any)
                        this.formErrors[field] = '';
                        var control = form.get(field);
                        if (control && control.dirty && !control.valid) {
                            var messages = this.validationMessages[field];
                            for (var key in control.errors) {
                                this.formErrors[field] += messages[key] + ' ';
                            }
                        }
                    }
                };
                AdminPermissionEditComponent.prototype.savePermission = function () {
                    var _this = this;
                    var formData = this.permissionForm.value;
                    if (this.permissionId === '0') {
                        this.dataService.add(formData).subscribe(function (permission) {
                            _this.alertService.success('Permission was added');
                            _this.permission = new permission_1.Permission();
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                    else {
                        this.dataService.update(this.permissionId, formData).subscribe(function (permission) {
                            _this.alertService.success('Permission was updated');
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                };
                AdminPermissionEditComponent.prototype.cancel = function () {
                    this.router.navigate(['/admin/permissions']);
                };
                AdminPermissionEditComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-permission-edit.component.html'
                    })
                ], AdminPermissionEditComponent);
                return AdminPermissionEditComponent;
            }());
            exports_1("AdminPermissionEditComponent", AdminPermissionEditComponent);
        }
    }
});
