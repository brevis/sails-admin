System.register("admin-roles.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var AdminRolesComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AdminRolesComponent = (function () {
                function AdminRolesComponent(dataService, alertService) {
                    this.dataService = dataService;
                    this.alertService = alertService;
                    this.currentRole = null;
                    this.loadRoles();
                }
                AdminRolesComponent.prototype.deleteRole = function (role) {
                    var _this = this;
                    var roleId = role.id;
                    this.dataService.destroy(role.id).subscribe(function (_role) {
                        if (_role && roleId === _role.id) {
                            _this.loadRoles();
                        }
                        else {
                            _this.alertService.error('Role with id "' + roleId + '" not found');
                        }
                        _this.currentRole = null;
                    });
                };
                AdminRolesComponent.prototype.loadRoles = function () {
                    var _this = this;
                    this.dataService.getAll().subscribe(function (roles) { _this.roles = roles; });
                };
                AdminRolesComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-roles.component.html'
                    })
                ], AdminRolesComponent);
                return AdminRolesComponent;
            }());
            exports_1("AdminRolesComponent", AdminRolesComponent);
        }
    }
});
