import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../_services/index';
import { User } from '../../_models/user';
import { Role } from '../../_models/role';
import { RolesDataService } from '../../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'admin-roles.component.html'
})

export class AdminRolesComponent {

    protected roles: Role[];

    protected currentRole: Role = null;

    constructor(
        private dataService: RolesDataService,
        private alertService: AlertService) {
        this.loadRoles();
    }

    deleteRole(role) {
        let roleId = role.id;
        this.dataService.destroy(role.id).subscribe(_role => {
            if (_role && roleId === _role.id) {
                this.loadRoles();
            } else {
                this.alertService.error('Role with id "' + roleId + '" not found');
            }

            this.currentRole = null;
        });
    }

    loadRoles() {
        this.dataService.getAll().subscribe(roles => { this.roles = roles; });
    }
}
