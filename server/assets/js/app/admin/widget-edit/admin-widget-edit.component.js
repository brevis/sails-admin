System.register("admin-widget-edit.component", ['@angular/core', '@angular/forms', '../../_models/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, forms_1, index_1;
    var AdminWidgetEditComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            AdminWidgetEditComponent = (function () {
                function AdminWidgetEditComponent(dataService, groupsDataService, location, router, route, alertService) {
                    this.dataService = dataService;
                    this.groupsDataService = groupsDataService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.widget = null;
                    this.groups = [];
                    this.widgetForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl(''),
                        code: new forms_1.FormControl(''),
                        enabled: new forms_1.FormControl(''),
                        groups: new forms_1.FormControl('')
                    });
                    this.formErrors = {
                        name: '',
                        code: '',
                        enabled: '',
                        groups: ''
                    };
                    this.validationMessages = {
                        name: {
                            required: 'Required.',
                            maxlength: 'Cannot be more than 30 characters long.'
                        },
                        code: {
                            maxlength: 'Cannot be more than 10000 characters long.'
                        }
                    };
                }
                AdminWidgetEditComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.loadWidget();
                    this.groupsDataService.getAll().subscribe(function (groups) {
                        _this.groups = groups;
                    });
                };
                AdminWidgetEditComponent.prototype.loadWidget = function () {
                    var _this = this;
                    this.widgetId = this.route.snapshot.params.id;
                    if (this.widgetId === '0') {
                        this.widget = new index_1.Widget();
                        this.buildForm();
                    }
                    else {
                        this.dataService.getOne(this.widgetId).subscribe(function (widget) {
                            _this.widget = widget;
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error("Widget with id \"" + _this.widgetId + "\" not found.");
                        });
                    }
                };
                AdminWidgetEditComponent.prototype.buildForm = function () {
                    var _this = this;
                    this.widgetForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl(this.widget.name, [forms_1.Validators.required, forms_1.Validators.maxLength(30)]),
                        code: new forms_1.FormControl(this.widget.code, [forms_1.Validators.maxLength(10000)]),
                        enabled: new forms_1.FormControl(this.widget.enabled),
                        groups: new forms_1.FormControl(this.widget.groups)
                    });
                    this.widgetForm.valueChanges
                        .subscribe(function (data) { return _this.onValueChanged(data); });
                    this.onValueChanged();
                };
                AdminWidgetEditComponent.prototype.onValueChanged = function (data) {
                    if (!this.widgetForm) {
                        return;
                    }
                    var form = this.widgetForm;
                    for (var field in this.formErrors) {
                        // clear previous error message (if any)
                        this.formErrors[field] = '';
                        var control = form.get(field);
                        if (control && control.dirty && !control.valid) {
                            var messages = this.validationMessages[field];
                            for (var key in control.errors) {
                                this.formErrors[field] += messages[key] + ' ';
                            }
                        }
                    }
                };
                AdminWidgetEditComponent.prototype.saveWidget = function () {
                    var _this = this;
                    var formData = this.widgetForm.value;
                    if (this.widgetId === '0') {
                        this.dataService.add(formData).subscribe(function (widget) {
                            _this.alertService.success('Widget was added');
                            _this.widget = new index_1.Widget();
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                    else {
                        this.dataService.update(this.widgetId, formData).subscribe(function (widget) {
                            _this.alertService.success('Widget was updated');
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                };
                AdminWidgetEditComponent.prototype.cancel = function () {
                    this.router.navigate(['/admin/widgets']);
                };
                AdminWidgetEditComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-widget-edit.component.html'
                    })
                ], AdminWidgetEditComponent);
                return AdminWidgetEditComponent;
            }());
            exports_1("AdminWidgetEditComponent", AdminWidgetEditComponent);
        }
    }
});
