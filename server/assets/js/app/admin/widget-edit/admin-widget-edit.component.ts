import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { Widget, Group } from '../../_models/index';
import { WidgetsDataService, GroupsDataService } from '../../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'admin-widget-edit.component.html'
})

export class AdminWidgetEditComponent implements OnInit {

    protected widgetId: string;

    protected widget: Widget = null;

    protected groups: Group[] = [];

    protected widgetForm: FormGroup = new FormGroup({
        name: new FormControl(''),
        code: new FormControl(''),
        enabled: new FormControl(''),
        groups: new FormControl(''),
    });

    protected formErrors = {
        name: '',
        code: '',
        enabled: '',
        groups: '',
    };

    protected validationMessages = {
        name: {
            required: 'Required.',
            maxlength: 'Cannot be more than 30 characters long.',
        },
        code: {
            maxlength: 'Cannot be more than 10000 characters long.',
        },
    };

    constructor(
        private dataService: WidgetsDataService,
        private groupsDataService: GroupsDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.loadWidget();
        this.groupsDataService.getAll().subscribe(groups => {
            this.groups = groups;
        });
    }

    loadWidget() {
        this.widgetId = this.route.snapshot.params.id;
        if (this.widgetId === '0') {
            this.widget = new Widget();
            this.buildForm();
        } else {
            this.dataService.getOne(this.widgetId).subscribe(widget => {
                this.widget = widget;
                this.buildForm();
            }, err => {
                this.alertService.error(`Widget with id "${this.widgetId}" not found.`);
            });
        }
    }

    buildForm() {
        this.widgetForm = new FormGroup({
            name: new FormControl(this.widget.name, [Validators.required, Validators.maxLength(30)]),
            code: new FormControl(this.widget.code, [Validators.maxLength(10000)]),
            enabled: new FormControl(this.widget.enabled),
            groups: new FormControl(this.widget.groups),
        });

        this.widgetForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.widgetForm) {
            return;
        }
        const form = this.widgetForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    saveWidget() {
        const formData = this.widgetForm.value;
        if (this.widgetId === '0') {
            this.dataService.add(formData).subscribe(widget => {
                this.alertService.success('Widget was added');
                this.widget = new Widget();
                this.buildForm();
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        } else {
            this.dataService.update(this.widgetId, formData).subscribe(widget => {
                this.alertService.success('Widget was updated');
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        }
    }

    cancel() {
        this.router.navigate(['/admin/widgets']);
    }

}
