import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { UsersDataService } from './users-data.service';
import { Promise } from 'es6-promise';
import { SavedUser } from '../_models/saved-user';
import { BaseAuthGuard } from "../_guards/base-auth.guard";

@Injectable()
export class AuthenticationService {

    public static readonly ROLE_PUBLIC: string = 'public';
    public static readonly ROLE_REGISTERED: string = 'registered';
    public static readonly ROLE_ADMIN: string = 'admin';

    public static readonly COOKIE_NAME: string = 'sails.sid';

    protected currentUser: SavedUser;

    constructor(private http: Http,
                private usersDataService: UsersDataService) {
        this.currentUser = AuthenticationService.getSavedUser();
    }

    public login(username: string, password: string): Observable<boolean> {
        return this.http.post('/auth/local', JSON.stringify({ identifier: username, password: password }))
            .map((response: Response) => {
                let user = response.json();
                if (user) {
                    this.currentUser = new SavedUser();
                    this.currentUser.id = user.id;
                    this.currentUser.username = username;
                    this.currentUser.token = btoa(username + ':' + password);
                    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                    return true;
                } else {
                    return false;
                }
            });
    }

    public logout(): void {
        this.currentUser = null;
        localStorage.removeItem('currentUser');
        document.cookie = AuthenticationService.COOKIE_NAME + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    public getCurrentUser(): Promise {
        if (!this.currentUser) {
            return new Promise((resolve, reject) => {
                resolve(null);
            });
        }

        if (this.currentUser.group && this.currentUser.roles && this.currentUser.permissions) {
            return new Promise((resolve, reject) => {
                resolve(this.currentUser);
            });
        }

        return new Promise((resolve, reject) => {
            return this.loadUserDetails().then(user => {
                this.currentUser.group = user.group;
                this.currentUser.roles = user.roles;
                this.currentUser.permissions = user.permission;
                this.currentUser.isAdmin = AuthenticationService.hasRoles(this.currentUser, [AuthenticationService.ROLE_ADMIN]);
                resolve(this.currentUser);
            });
        });
    }

    public static getToken(): string {
        let currentUser = AuthenticationService.getSavedUser();
        return (currentUser && currentUser.token) || '';
    }

    public static getSavedUser(): SavedUser {
        let savedUser: SavedUser;
        try {
            savedUser = JSON.parse(localStorage.getItem('currentUser'));
        } catch (e) {
            savedUser = null;
        }
        return (savedUser && savedUser.id) ? savedUser : null;
    }

    public static hasRoles(user: any, roles: Array, every: boolean = false) {
        if (!user || !user.token || !user.roles) {
            return false;
        }

        let rolesIntersect = [];
        user.roles.forEach((role) => {
            if (roles.indexOf(role.name) != -1) {
                rolesIntersect.push(role.name);
            }
        });

        if (every) {
            return rolesIntersect.length = roles.length;
        } else {
            return rolesIntersect.length > 0;
        }
    }

    protected loadUserDetails() {
        if (!this.currentUser || !this.currentUser.id) {
            return null;
        }

        return new Promise((resolve, reject) => {
            this.usersDataService.getUserDetails(this.currentUser.id).subscribe(user => {
                resolve(user);
            });
        });
    }

}