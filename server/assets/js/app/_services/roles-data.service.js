System.register("roles-data.service", ['@angular/core', './base-data.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, base_data_service_1;
    var RolesDataService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (base_data_service_1_1) {
                base_data_service_1 = base_data_service_1_1;
            }],
        execute: function() {
            RolesDataService = (function (_super) {
                __extends(RolesDataService, _super);
                function RolesDataService() {
                    _super.apply(this, arguments);
                    this.modelName = 'role';
                }
                RolesDataService = __decorate([
                    core_1.Injectable()
                ], RolesDataService);
                return RolesDataService;
            }(base_data_service_1.BaseDataService));
            exports_1("RolesDataService", RolesDataService);
        }
    }
});
