System.register("widgets-data.service", ['@angular/core', './base-data.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, base_data_service_1;
    var WidgetsDataService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (base_data_service_1_1) {
                base_data_service_1 = base_data_service_1_1;
            }],
        execute: function() {
            WidgetsDataService = (function (_super) {
                __extends(WidgetsDataService, _super);
                function WidgetsDataService(http, rawsettingsDataService) {
                    _super.call(this, http);
                    this.http = http;
                    this.rawsettingsDataService = rawsettingsDataService;
                    this.modelName = 'widget';
                }
                /**
                 * Get widgets placed by columns
                 *
                 * @param group
                 * @param userId
                 * @returns {Promise}
                 */
                WidgetsDataService.prototype.getWidgetsPositions = function (group, userId) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        // get widgets for group (or all, if group == SETTINGS_KEY_GROUP_ALL)
                        var where = {};
                        if (group != WidgetsDataService.SETTINGS_KEY_GROUP_ALL) {
                            where.group = group;
                        }
                        _this.find(where).subscribe(function (widgets) {
                            var indexedWidgets = WidgetsDataService.indexObjects(widgets);
                            if (!userId) {
                                userId = group;
                            }
                            // get widgets positions for group
                            _this.rawsettingsDataService.find({ name: WidgetsDataService.getSettingsKey(userId) }).subscribe(function (widgetsPositions) {
                                try {
                                    widgetsPositions = JSON.parse(widgetsPositions[0].data);
                                }
                                catch (e) {
                                    widgetsPositions = null;
                                }
                                if (userId && widgetsPositions === null) {
                                    resolve(null);
                                }
                                if (!widgetsPositions) {
                                    widgetsPositions = [];
                                }
                                var columns = WidgetsDataService.getEmptyColumns(Math.max(widgetsPositions.length, WidgetsDataService.COLUMNS_COUNT));
                                // place widgets to its places
                                widgetsPositions.forEach(function (positions, column) {
                                    positions.forEach(function (pos) {
                                        if (indexedWidgets[pos.id]) {
                                            columns[column].push(indexedWidgets[pos.id]);
                                            indexedWidgets[pos.id] = null;
                                        }
                                    });
                                });
                                // place unplaced widgets to column[0]
                                for (var id in indexedWidgets) {
                                    if (indexedWidgets[id]) {
                                        columns[0].push(indexedWidgets[id]);
                                    }
                                }
                                resolve(columns);
                            });
                        });
                    });
                };
                WidgetsDataService.prototype.saveWidgetsPositions = function (group, columns) {
                    columns = columns.map(function (column) {
                        return column.map(function (widget) {
                            return { id: widget.id };
                        });
                    });
                    return this.rawsettingsDataService.saveSettings(WidgetsDataService.getSettingsKey(group), JSON.stringify(columns));
                };
                WidgetsDataService.getSettingsKey = function (group) {
                    return WidgetsDataService.SETTINGS_KEY
                        .replace('{group}', group);
                };
                WidgetsDataService.getEmptyColumns = function (count) {
                    var columns = [];
                    for (var i = 0; i < count; i++) {
                        columns.push([]);
                    }
                    return columns;
                };
                WidgetsDataService.indexObjects = function (objects, key) {
                    if (key === void 0) { key = 'id'; }
                    var indexedObjects = {};
                    objects.forEach(function (item) {
                        indexedObjects[item[key]] = item;
                    });
                    return indexedObjects;
                };
                WidgetsDataService.readonly = COLUMNS_COUNT = 3;
                WidgetsDataService.readonly = SETTINGS_KEY_GROUP_ALL = 'all';
                WidgetsDataService.readonly = SETTINGS_KEY = 'wigetspositions_{group}';
                WidgetsDataService = __decorate([
                    core_1.Injectable()
                ], WidgetsDataService);
                return WidgetsDataService;
            }(base_data_service_1.BaseDataService));
            exports_1("WidgetsDataService", WidgetsDataService);
        }
    }
});
