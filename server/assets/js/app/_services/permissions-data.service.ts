import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';

@Injectable()
export class PermissionsDataService extends BaseDataService {

    protected modelName: string = 'permission';

}