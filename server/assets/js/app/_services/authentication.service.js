System.register("authentication.service", ['@angular/core', 'rxjs/add/operator/map', 'es6-promise', '../_models/saved-user'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, es6_promise_1, saved_user_1;
    var AuthenticationService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (_1) {},
            function (es6_promise_1_1) {
                es6_promise_1 = es6_promise_1_1;
            },
            function (saved_user_1_1) {
                saved_user_1 = saved_user_1_1;
            }],
        execute: function() {
            AuthenticationService = (function () {
                function AuthenticationService(http, usersDataService) {
                    this.http = http;
                    this.usersDataService = usersDataService;
                    this.string = 'public';
                    this.string = 'registered';
                    this.string = 'admin';
                    this.string = 'sails.sid';
                    this.currentUser = AuthenticationService.getSavedUser();
                }
                AuthenticationService.prototype.login = function (username, password) {
                    var _this = this;
                    return this.http.post('/auth/local', JSON.stringify({ identifier: username, password: password }))
                        .map(function (response) {
                        var user = response.json();
                        if (user) {
                            _this.currentUser = new saved_user_1.SavedUser();
                            _this.currentUser.id = user.id;
                            _this.currentUser.username = username;
                            _this.currentUser.token = btoa(username + ':' + password);
                            localStorage.setItem('currentUser', JSON.stringify(_this.currentUser));
                            return true;
                        }
                        else {
                            return false;
                        }
                    });
                };
                AuthenticationService.prototype.logout = function () {
                    this.currentUser = null;
                    localStorage.removeItem('currentUser');
                    document.cookie = AuthenticationService.COOKIE_NAME + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                };
                AuthenticationService.prototype.getCurrentUser = function () {
                    var _this = this;
                    if (!this.currentUser) {
                        return new es6_promise_1.Promise(function (resolve, reject) {
                            resolve(null);
                        });
                    }
                    if (this.currentUser.group && this.currentUser.roles && this.currentUser.permissions) {
                        return new es6_promise_1.Promise(function (resolve, reject) {
                            resolve(_this.currentUser);
                        });
                    }
                    return new es6_promise_1.Promise(function (resolve, reject) {
                        return _this.loadUserDetails().then(function (user) {
                            _this.currentUser.group = user.group;
                            _this.currentUser.roles = user.roles;
                            _this.currentUser.permissions = user.permission;
                            _this.currentUser.isAdmin = AuthenticationService.hasRoles(_this.currentUser, [AuthenticationService.ROLE_ADMIN]);
                            resolve(_this.currentUser);
                        });
                    });
                };
                AuthenticationService.getToken = function () {
                    var currentUser = AuthenticationService.getSavedUser();
                    return (currentUser && currentUser.token) || '';
                };
                AuthenticationService.getSavedUser = function () {
                    var savedUser;
                    try {
                        savedUser = JSON.parse(localStorage.getItem('currentUser'));
                    }
                    catch (e) {
                        savedUser = null;
                    }
                    return (savedUser && savedUser.id) ? savedUser : null;
                };
                AuthenticationService.hasRoles = function (user, roles, every) {
                    if (every === void 0) { every = false; }
                    if (!user || !user.token || !user.roles) {
                        return false;
                    }
                    var rolesIntersect = [];
                    user.roles.forEach(function (role) {
                        if (roles.indexOf(role.name) != -1) {
                            rolesIntersect.push(role.name);
                        }
                    });
                    if (every) {
                        return rolesIntersect.length = roles.length;
                    }
                    else {
                        return rolesIntersect.length > 0;
                    }
                };
                AuthenticationService.prototype.loadUserDetails = function () {
                    var _this = this;
                    if (!this.currentUser || !this.currentUser.id) {
                        return null;
                    }
                    return new es6_promise_1.Promise(function (resolve, reject) {
                        _this.usersDataService.getUserDetails(_this.currentUser.id).subscribe(function (user) {
                            resolve(user);
                        });
                    });
                };
                AuthenticationService.readonly = ROLE_PUBLIC;
                AuthenticationService.readonly = ROLE_REGISTERED;
                AuthenticationService.readonly = ROLE_ADMIN;
                AuthenticationService.readonly = COOKIE_NAME;
                AuthenticationService = __decorate([
                    core_1.Injectable()
                ], AuthenticationService);
                return AuthenticationService;
            }());
            exports_1("AuthenticationService", AuthenticationService);
        }
    }
});
