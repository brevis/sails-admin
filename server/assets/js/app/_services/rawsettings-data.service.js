System.register("rawsettings-data.service", ['@angular/core', './base-data.service', '../_models/rawsettings'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, base_data_service_1, rawsettings_1;
    var RawsettingsDataService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (base_data_service_1_1) {
                base_data_service_1 = base_data_service_1_1;
            },
            function (rawsettings_1_1) {
                rawsettings_1 = rawsettings_1_1;
            }],
        execute: function() {
            RawsettingsDataService = (function (_super) {
                __extends(RawsettingsDataService, _super);
                function RawsettingsDataService() {
                    _super.apply(this, arguments);
                    this.modelName = 'rawsettings';
                }
                RawsettingsDataService.prototype.saveSettings = function (key, data) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        _this.find({ name: key }).subscribe(function (settings) {
                            var model = new rawsettings_1.Rawsettings();
                            model.name = key;
                            model.data = data;
                            if (settings && settings[0] && settings[0].id) {
                                _this.update(settings[0].id, model).subscribe(function (_) {
                                    resolve(true);
                                }, function (err) { return resolve(false); });
                            }
                            else {
                                _this.add(model).subscribe(function (_) {
                                    resolve(true);
                                }, function (err) { return resolve(false); });
                            }
                        }, function (err) { return resolve(false); });
                    });
                };
                RawsettingsDataService = __decorate([
                    core_1.Injectable()
                ], RawsettingsDataService);
                return RawsettingsDataService;
            }(base_data_service_1.BaseDataService));
            exports_1("RawsettingsDataService", RawsettingsDataService);
        }
    }
});
