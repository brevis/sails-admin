System.register("index", ['./user', './saved-user', './group', './role', './permission', './widget', './rawsettings'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for(var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters:[
            function (user_1_1) {
                exportStar_1(user_1_1);
            },
            function (saved_user_1_1) {
                exportStar_1(saved_user_1_1);
            },
            function (group_1_1) {
                exportStar_1(group_1_1);
            },
            function (role_1_1) {
                exportStar_1(role_1_1);
            },
            function (permission_1_1) {
                exportStar_1(permission_1_1);
            },
            function (widget_1_1) {
                exportStar_1(widget_1_1);
            },
            function (rawsettings_1_1) {
                exportStar_1(rawsettings_1_1);
            }],
        execute: function() {
        }
    }
});
