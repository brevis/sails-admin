import { User } from './user'

export class Group {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    users: User[];
    reserved: boolean;
}