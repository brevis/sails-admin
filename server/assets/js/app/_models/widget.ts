import { User } from './user'
import { Group } from './group'

export class Widget {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    code: string;
    enabled: boolean;
    users: User[];
    groups: Group[];

}