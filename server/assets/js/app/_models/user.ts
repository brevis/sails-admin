import { Group } from './group'
import { Role } from './role'
import { Permission } from './permission'
import { Widget } from './widget'

export class User {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    username: string;
    email: string;
    password: string;
    group: Group[];
    roles: Role[];
    permission: Permission[];
    widgets: Widget[];
}
