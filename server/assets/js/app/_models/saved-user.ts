import { Group } from './group'
import { Role } from './role'
import { Permission } from './permission'

export class SavedUser {
    id: string;
    username: string;
    token: string;
    group: Group;
    roles: Role[];
    permissions: Permission[];
    isAdmin: boolean = false;
}
