import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService, AlertService } from '../_services/index';
import { BaseAuthGuard } from "./base-auth.guard";

@Injectable()
export class AdminAuthGuard extends BaseAuthGuard implements CanActivate {

    protected redirectUrl = ['/auth/login'];
    protected redirectMessage = 'Access denied';
    protected roles = [AuthenticationService.ROLE_ADMIN];
    protected rolesEvery = false;

}