import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BaseAuthGuard } from './base-auth.guard';
import { AuthenticationService, AlertService } from '../_services/index';

@Injectable()
export class RegisteredAuthGuard extends BaseAuthGuard implements CanActivate {

    protected redirectUrl = ['/'];
    protected redirectMessage = 'Access denied';
    protected roles = [AuthenticationService.ROLE_REGISTERED, AuthenticationService.ROLE_ADMIN];
    protected rolesEvery = false;

}
