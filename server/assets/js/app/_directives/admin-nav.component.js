System.register("admin-nav.component", ['@angular/core', '../_services/authentication.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, authentication_service_1;
    var AdminNavComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (authentication_service_1_1) {
                authentication_service_1 = authentication_service_1_1;
            }],
        execute: function() {
            AdminNavComponent = (function () {
                function AdminNavComponent(authService) {
                    this.authService = authService;
                }
                AdminNavComponent.prototype.ngOnInit = function () {
                    this.user = authentication_service_1.AuthenticationService.getSavedUser();
                };
                AdminNavComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        selector: 'admin-nav',
                        templateUrl: 'admin-nav.component.html',
                        styleUrls: ['admin-nav.component.css']
                    })
                ], AdminNavComponent);
                return AdminNavComponent;
            }());
            exports_1("AdminNavComponent", AdminNavComponent);
        }
    }
});
