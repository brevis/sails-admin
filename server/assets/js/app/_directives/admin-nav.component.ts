import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { SavedUser } from "../_models/saved-user";


@Component({
    moduleId: module.id,
    selector: 'admin-nav',
    templateUrl: 'admin-nav.component.html',
    styleUrls: ['admin-nav.component.css'],
})

export class AdminNavComponent implements OnInit {

    protected user: SavedUser;

    constructor (protected authService: AuthenticationService) {

    }

    ngOnInit() {
        this.user = AuthenticationService.getSavedUser();
    }

}