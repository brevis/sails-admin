System.register("frontend-nav.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var FrontendNavComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            FrontendNavComponent = (function () {
                function FrontendNavComponent(authService, ref) {
                    this.authService = authService;
                    this.ref = ref;
                    this.user = null;
                }
                FrontendNavComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.authService.getCurrentUser().then(function (currentUser) {
                        _this.user = currentUser;
                        if (!_this.user || !_this.user.token) {
                            _this.user = null;
                        }
                    });
                };
                FrontendNavComponent.prototype.isAdminLogged = function () {
                    return this.user && this.user.isAdmin;
                };
                FrontendNavComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        selector: 'frontend-nav',
                        templateUrl: 'frontend-nav.component.html',
                        styleUrls: ['frontend-nav.component.css']
                    })
                ], FrontendNavComponent);
                return FrontendNavComponent;
            }());
            exports_1("FrontendNavComponent", FrontendNavComponent);
        }
    }
});
