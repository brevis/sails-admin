import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { BaseAuthGuard } from '../_guards/base-auth.guard';

@Component({
    moduleId: module.id,
    selector: 'frontend-nav',
    templateUrl: 'frontend-nav.component.html',
    styleUrls: ['frontend-nav.component.css'],
})

export class FrontendNavComponent implements OnInit {

    protected user: object = null;

    constructor (private authService: AuthenticationService,
                 private ref:ChangeDetectorRef) {

    }

    ngOnInit() {
        this.authService.getCurrentUser().then(currentUser => {
            this.user = currentUser;
            if (!this.user || !this.user.token) {
                this.user = null;
            }
        });
    }

    public isAdminLogged(): boolean {
        return this.user && this.user.isAdmin;
    }

}