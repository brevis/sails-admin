module.exports = function(grunt) {

    grunt.config.set('babel', {
        dev: {
            files: [{
                expand: true,
                cwd: '.tmp/public/concat',
                src: ['*.js', '!dependencies/**/*.js'],
                dest: '.tmp/public/dist',
                ext: '.js'
            }]
        }
    });

    grunt.loadNpmTasks('grunt-babel');
};