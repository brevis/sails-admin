/**
 * `copy`
 *
 * ---------------------------------------------------------------
 *
 * Copy files and/or folders from your `assets/` directory into
 * the web root (`.tmp/public`) so they can be served via HTTP,
 * and also for further pre-processing by other Grunt tasks.
 *
 * #### Normal usage (`sails lift`)
 * Copies all directories and files (except CoffeeScript and LESS)
 * from the `assets/` folder into the web root -- conventionally a
 * hidden directory located `.tmp/public`.
 *
 * #### Via the `build` tasklist (`sails www`)
 * Copies all directories and files from the .tmp/public directory into a www directory.
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-copy
 *
 */
module.exports = function (grunt) {

    grunt.config.set('copy', {
        dev: {
            files: [{
                expand: true,
                cwd: './assets',
                src: ['**/*.!(coffee|less)'],
                dest: '.tmp/public'
            }]
        },
        build: {
            files: [
                {
                    expand: true,
                    cwd: '../client-angular/dist/',
                    src: ['**/*'],
                    dest: '.tmp/public'
                },
                {
                    src: '../client-angular/dist/index.html',
                    dest: 'views/layout.ejs'
                }
            ],
        },

        // dev_angular2: {
        //     files: [{
        //         expand: true,
        //         src: [
        //             './node_modules/core-js/client/shim.min.js',
        //             './node_modules/es6-shim/es6-shim.min.js',
        //             './node_modules/systemjs/dist/system-polyfills.js',
        //             './node_modules/systemjs/dist/system.src.js',
        //             './node_modules/zone.js/dist/zone.min.js',
        //             './node_modules/reflect-metadata/Reflect.js',
        //             './node_modules/rxjs/**/*.js',
        //             './node_modules/@angular/**/*.css',
        //
        //             './node_modules/plugin-typescript/lib/plugin.js',
        //             './node_modules/typescript/lib/typescript.js',
        //
        //             './node_modules/@angular/common/bundles/common.umd.js',
        //             './node_modules/@angular/compiler/bundles/compiler.umd.js',
        //             './node_modules/@angular/core/bundles/core.umd.js',
        //             './node_modules/@angular/forms/bundles/forms.umd.js',
        //             './node_modules/@angular/http/bundles/http.umd.js',
        //             './node_modules/@angular/router/bundles/router.umd.js',
        //             './node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
        //             './node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
        //             './node_modules/@angular/material/bundles/material.umd.js',
        //             './node_modules/@angular/animations/bundles/animations.umd.js',
        //             './node_modules/@angular/animations/bundles/animations-browser.umd.js',
        //             './node_modules/@angular/platform-browser/bundles/platform-browser-animations.umd.js',
        //             './node_modules/@angular/platform-browser/animations.js',
        //
        //             './node_modules/es6-promise/dist/es6-promise.min.js',
        //             './node_modules/ng2-dnd/bundles/index.umd.js',
        //             './node_modules/ng2-dnd/style.css',
        //
        //         ],
        //         dest: '.tmp/public/js'
        //     },
        //
        //     // TODO: css in .tmp/public/styles
        //
        //     ]
        // }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
};
