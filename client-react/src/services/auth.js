import axios from 'axios';
import HttpService from './http';

export default class AuthService {

    static LOCALSTORAGE_KEY = 'authuser';
    static _isAuth = false;

    static login(credentials) {

        return new Promise((resolve, reject) => {
            axios.post(HttpService.API_HOST + '/auth/local', credentials, {
                headers: {
                    'Content-type': 'text/plain'
                }
            })
            .then(function (response) {
                let user = response.data;
                if (user) {
                    resolve(user);
                    AuthService._isAuth = true;
                    user.token = btoa(user.username + ':' + credentials.password);
                    localStorage.setItem(AuthService.LOCALSTORAGE_KEY, JSON.stringify(user));
                }
            })
            .catch(function (error) {
                reject(error);
                console.log(error);
            });
        });
    }

    static logout () {
        // TODO: clear auth;
        AuthService._isAuth = false;
        localStorage.removeItem(AuthService.LOCALSTORAGE_KEY)
        return new Promise((resolve, reject) => {
            resolve();
        })
    }

    static getCurrentUser() {
        return JSON.parse(localStorage.getItem(AuthService.LOCALSTORAGE_KEY));
    }

    static isAuth() {
        let currentUser = AuthService.getCurrentUser();
        return currentUser && currentUser.id && currentUser.token;
    }

    static userId() {
        let currentUser = AuthService.getCurrentUser();
        return currentUser && currentUser.id ? currentUser.id : null;
    }

    static getToken() {
        let user = AuthService.getCurrentUser();
        if (user && user.token) {
            return user.token;
        }
    }

}

