import HttpService from './http';
import AuthService from './auth';
import axios from 'axios';

export default class RawsettingsService extends HttpService {


    static saveSettings(key, data) {
        return new Promise((resolve, reject) => {

            axios.get(HttpService.API_HOST + '/api/rawsettings?where=' + JSON.stringify({name: key}), {
                headers: {
                    'Content-type': 'text/plain',
                    'Authorization': 'Basic ' + AuthService.getToken()
                }
            }).then(response => {
                let model = {
                    name: key,
                    data: data
                };

                if (response.data.length > 0) {
                    axios.put(HttpService.API_HOST + '/api/rawsettings/' + response.data[0].id, model, {
                        headers: {
                            'Content-type': 'text/plain',
                            'Authorization': 'Basic ' + AuthService.getToken()
                        }
                    }).then(response => {
                        resolve(true);
                    });
                } else {
                    axios.post(HttpService.API_HOST + '/api/rawsettings', model, {
                        headers: {
                            'Content-type': 'text/plain',
                            'Authorization': 'Basic ' + AuthService.getToken()
                        }
                    }).then(response => {
                        resolve(true);
                    });
                }

            });
        });
    }

}

