import axios from 'axios';
import RawsettingsService from './rawsettings';
import AuthService from './auth';
import HttpService from './http';

export default class WidgetsService extends HttpService {

    static SETTINGS_KEY_GROUP_ALL = 'all';
    static DEFAULT_WIDTH = 3;
    static DEFAULT_HEIGHT = 5;
    static GRID_COLS = 12;
    static GRID_ROW_HEIGHT = 30;
    static GRID_WIDTH_PX = 1160;
    static SETTINGS_KEY = 'wigetspositions_{group}';

    static getWidgetsPositions(group, userId) {

        return new Promise((resolve, reject) => {

            // get widgets for group (or all, if group == SETTINGS_KEY_GROUP_ALL)
            let where = {};
            if (group !== WidgetsService.SETTINGS_KEY_GROUP_ALL) {
                where.group = group;
            }

            axios.get(HttpService.API_HOST + '/api/widget?where=' + JSON.stringify(where), {
                headers: {
                    'Content-type': 'text/plain',
                    'Authorization': 'Basic ' + AuthService.getToken()
                }
            })
            .then((response) => {
                let widgets;
                try {
                    widgets = response.data;
                } catch (e) {
                    widgets = null;
                }

                if (!widgets) {
                    resolve(null);
                    return;
                }

                let indexedWidgets = WidgetsService.indexObjects(widgets);
                let rawsettingsKey = userId;
                if (!rawsettingsKey) {
                    rawsettingsKey = group;
                }

                // TODO: move to Rawsettings?:

                // get widgets positions for group
                where = {name: WidgetsService.getSettingsKey(rawsettingsKey)};
                axios.get(HttpService.API_HOST + '/api/rawsettings?where=' + JSON.stringify(where), {
                    headers: {
                        'Content-type': 'text/plain',
                        'Authorization': 'Basic ' + AuthService.getToken()
                    }
                }).then(response => {

                    let widgetsPositions;
                    try {
                        widgetsPositions = JSON.parse(response.data[0].data);
                    } catch (e) {
                        widgetsPositions = null;
                    }

                    if (userId && widgetsPositions === null) {
                        resolve(null);
                        return;
                    }

                    if (!widgetsPositions) {
                        widgetsPositions = [];
                    }

                    let positions = [];

                    // place widgets to its places
                    widgetsPositions.forEach((pos) => {
                        if (indexedWidgets[pos.i]) {
                            let widget = indexedWidgets[pos.i];

                            if (!pos.w || !pos.h) {
                                pos.w = widget.minWidth;
                                pos.h = widget.minHeight;
                            }

                            pos.minW = widget.minWidth;
                            pos.minH = widget.minHeight;
                            pos.maxW = widget.maxWidth;
                            pos.maxH = widget.maxHeight;

                            if (pos.w < pos.minW) {
                                pos.w = pos.minW;
                            }

                            if (pos.h < pos.minH) {
                                pos.h = pos.minH;
                            }

                            if (pos.w > pos.maxW) {
                                pos.w = pos.maxW;
                            }

                            if (pos.h > pos.maxH) {
                                pos.h = pos.maxH;
                            }

                            pos.isDraggable = true;
                            pos.isResizable = true;

                            widget.position = pos;
                            positions.push(widget);
                            indexedWidgets[pos.i] = null;
                        }
                    });

                    // place unplaced widgets to column[0]
                    for (let id in indexedWidgets) {
                        if (indexedWidgets[id]) {
                            let widget = indexedWidgets[id],
                                width = WidgetsService.DEFAULT_WIDTH,
                                height = WidgetsService.DEFAULT_HEIGHT;

                            if (widget.width && widget.height) {
                                width = parseInt(widget.width, 10);
                                height = parseInt(widget.height, 10);
                            }

                            widget.position = {
                                i: widget.id,
                                x: 0,
                                y: 0,
                                w: widget.minWidth,
                                h: widget.minHeight,
                                minW: widget.minWidth,
                                minH: widget.minHeight,
                                maxW: widget.maxWidth,
                                maxH: widget.maxHeight,
                                isDraggable: true,
                                isResizable: true,
                            };

                            widget.position.w = Math.min(widget.position.minW, widget.position.w);
                            widget.position.h = Math.min(widget.position.minH, widget.position.h);

                            positions.push(widget);
                        }
                    }

                    resolve(positions);
                });

            });
        });
    }

    static saveWidgetsPositions (group, positions) {
        // console.log(positions);
        // positions = positions.map(widget => {
        //     return {id: widget.id, position: widget.position};
        // });
        return RawsettingsService.saveSettings(WidgetsService.getSettingsKey(group), JSON.stringify(positions));
    }

    static getSettingsKey(group) {
        return WidgetsService.SETTINGS_KEY
            .replace('{group}', group);
    }

    static getEmptyColumns(count) {
        let columns = [];
        for (let i = 0; i < count; i++) {
            columns.push([]);
        }
        return columns;
    }

    static indexObjects(objects, key) {
        if (!key) {
            key = 'id';
        }
        let indexedObjects = {};
        objects.forEach(item => {
            indexedObjects[item[key]] = item;
        });
        return indexedObjects;
    }

    static getPositionSize(widget, pos) {
        if (!pos) {
            pos = {};
        }

        if (widget && widget.size) {
            if (!pos.size) {
                pos.size = widget.size.split(',')[0]
            }
        }

        if (!pos.size) {
            pos.size = WidgetsService.DEFAULT_WIDTH + 'x' + WidgetsService.DEFAULT_HEIGHT;
        }

        return pos.size;
    }

}

