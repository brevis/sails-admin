import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AuthService from '../../../services/auth';

export default class TopNav extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                            <ul className="nav navbar-nav">
                                <li><Link to="/">Главная</Link></li>
                                {AuthService.isAuth() ? <li><Link to="/widgets">Виджеты</Link></li> : ''}
                            </ul>
                            {
                                !AuthService.isAuth()
                                    ?
                                    <ul className="nav navbar-nav navbar-right">
                                        <li><Link to="/auth/login">Вход</Link></li>
                                    </ul>
                                    : ''
                            }
                            {
                                AuthService.isAuth ()
                                    ? <ul className="nav navbar-nav navbar-right">
                                            <li><a>Привет, {AuthService.getCurrentUser().username}</a></li>
                                            <li className="hidden"><Link to="/admin">Админпанель</Link></li>
                                            <li><Link to="/auth/logout">Выход</Link></li>
                                        </ul>
                                    : ''
                            }
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}
