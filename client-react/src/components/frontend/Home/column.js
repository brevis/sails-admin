import React, {Component} from 'react';
export default class Column extends Component {

    render() {

        let widgets = this.props.data;
        let widgetsTemplate = widgets.map(function(widget, index) {
            return (
                <div className="widget" key={index} dangerouslySetInnerHTML={{__html: widget.code}}/>
            )
        });

        return (
            <div className="col-md-4">
                {widgetsTemplate}
            </div>
        );
    }
}