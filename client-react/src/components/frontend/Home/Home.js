import React, {Component} from 'react';
import TopNav from '../topnav/topnav';
import Widgets from './widgets';

export default class HomePage extends Component {
    render() {
        return (
            <div>
                <TopNav/>
                <div className="jumbotron">
                    <h1>Главная страница</h1>
                    <p>(реальный контент появится позже)</p>
                </div>
                <Widgets/>
            </div>
        );
    }
}