import React, {Component} from 'react';
import AuthService from '../../../services/auth';
import WidgetsService from '../../../services/widgets';
import ReactGridLayout from 'react-grid-layout';
import 'react-grid-layout/css/styles.css';

export default class Widgets extends Component {

    constructor(props) {
        super(props);
        this.state = {
            positions: []
        }
    }

    getInitialState() {
        return {
            positions: []
        };
    }

    componentDidMount() {
        let self = this;

        WidgetsService.getWidgetsPositions(WidgetsService.SETTINGS_KEY_GROUP_ALL, AuthService.userId()).then(positions => {
            if (positions) {
                self.setState({positions: positions});
            } else {
                WidgetsService.getWidgetsPositions(WidgetsService.SETTINGS_KEY_GROUP_ALL).then(positions => {
                    self.setState({positions: positions});
                });
            }
        });
    }

    render() {

        let widgetsTemplate = this.state.positions.map(function(widget, index) {
            let pos = widget.position;
            pos.isDraggable = false;
            pos.isResizable = false;
            return (
                <div className="panel panel-default draggable" key={widget.id} data-grid={pos}>
                    <div className="panel-heading">{widget.name}</div>
                    <div className="panel-body" dangerouslySetInnerHTML={{__html: widget.code}}/>
                </div>
            )
        });

        return (
            <ReactGridLayout className="layout" cols={WidgetsService.GRID_COLS} rowHeight={WidgetsService.GRID_ROW_HEIGHT} width={WidgetsService.GRID_WIDTH_PX}>
                {widgetsTemplate}
            </ReactGridLayout>
        );
    }
}