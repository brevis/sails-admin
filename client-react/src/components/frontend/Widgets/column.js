import React, {Component} from 'react';
import Sortable from 'sortablejs';

export default class Column extends Component {

    sortableGroupDecorator = (componentBackingInstance) => {
        if (componentBackingInstance) {
            let options = {
                group: "shared",
            };
            Sortable.create(componentBackingInstance, options);
        }
    };

    render() {

        let widgets = this.props.data;
        let widgetsTemplate = widgets.map(function(widget, index) {
            return (
                <div className="panel panel-default draggable" key={index} data-id={widget.id} data-grid={{x: 0, y: 0, w: 1, h: 2}}>
                    <div className="panel-heading">{widget.name}</div>
                    <div className="panel-body" dangerouslySetInnerHTML={{__html: widget.code}}/>
                </div>
            )
        });

        return (
            <div className="col-md-4 panel panel-warning draggable-widgets-column">
                <div className="panel-body" ref={this.sortableGroupDecorator}>
                    {widgetsTemplate}
                </div>
            </div>
        );
    }

}