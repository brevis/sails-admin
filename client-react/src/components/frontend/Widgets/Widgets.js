import React, {Component} from 'react';
import TopNav from '../topnav/topnav';
import AuthService from '../../../services/auth';
import WidgetsService from '../../../services/widgets';
import NotificationSystem from 'react-notification-system';
import ReactGridLayout from 'react-grid-layout';
import 'react-grid-layout/css/styles.css';

export default class WidgetsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            widgets: [],
            positions: []
        };

        this.notificationSystem = null;
        this.savePosition = this.savePosition.bind(this);
        this.updateLayoutState = this.updateLayoutState.bind(this);
    }

    getInitialState() {
        return {
            widgets: [],
            positions: []
        };
    }

    componentDidMount() {
        let self = this;

        WidgetsService.getWidgetsPositions(WidgetsService.SETTINGS_KEY_GROUP_ALL, AuthService.userId()).then(widgets => {
            if (widgets) {
                self.setState({widgets: widgets});
            } else {
                WidgetsService.getWidgetsPositions(WidgetsService.SETTINGS_KEY_GROUP_ALL).then(widgets => {
                    self.setState({widgets: widgets});
                });
            }
        });

        this.notificationSystem = this.refs.notificationSystem;
    }

    savePosition(event) {
        if (event) {
            event.preventDefault();
        }

        WidgetsService.saveWidgetsPositions(AuthService.getCurrentUser().id, this.state.positions);

        this.notificationSystem.addNotification({
            message: 'Расположение виджетов сохранено',
            level: 'success'
        });
    }

    updateLayoutState(layout) {
        this.setState({positions: layout});
    }

    render() {
        let widgetsTemplate = this.state.widgets.map((widget) => {
            return (
                <div className="panel panel-default draggable" key={widget.id} data-grid={widget.position}>
                    <div className="panel-heading">{widget.name}</div>
                    <div className="panel-body" dangerouslySetInnerHTML={{__html: widget.code}}/>
                </div>
            )
        });

        return (
            <div>
                <TopNav/>
                <div className="row page-header">
                    <div className="col-md-6">
                        <h2>Виджеты</h2>
                    </div>
                    <div className="col-md-6 text-right">
                        <button type="button" className="btn btn-success" onClick={this.savePosition}>Сохранить расположение</button>
                    </div>
                </div>

                <ReactGridLayout className="layout" cols={WidgetsService.GRID_COLS} rowHeight={WidgetsService.GRID_ROW_HEIGHT} width={WidgetsService.GRID_WIDTH_PX} onLayoutChange={this.updateLayoutState}>
                    {widgetsTemplate}
                </ReactGridLayout>

                <NotificationSystem ref="notificationSystem" />
            </div>
        );
    }
}