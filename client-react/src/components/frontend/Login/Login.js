import React, {Component} from 'react';
import AuthService from '../../../services/auth';
import NotificationSystem from 'react-notification-system';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

export default class LoginPage extends Component {

    static contextTypes = {
        router: React.PropTypes.shape({
            history: React.PropTypes.object.isRequired,
        }),
    };

    constructor(props) {
        super(props);
        this.state = {
            errors: {},
            credentials: {
                identifier: '',
                password: ''
            }
        };

        this.notificationSystem = null;

        this.login = this.login.bind(this);
        this.cancel = this.cancel.bind(this);
        this.onChange = this.onChange.bind(this);

    }

    componentDidMount() {
        this.notificationSystem = this.refs.notificationSystem;
    }

    login(event) {
        event.preventDefault();

        // create a string for an HTTP body message
        const identifier = encodeURIComponent(this.state.credentials.identifier);
        const password = encodeURIComponent(this.state.credentials.password);

        // TODO: move to some service:
        AuthService.login({
            identifier: identifier,
            password: password
        }).then(user => {
            if (user && user.id) {
                this.setState({
                    errors: {}
                });
                this.context.router.history.push('/')
            } else {
                this.notificationSystem.addNotification({
                    message: 'Имя пользователя или Пароль неверны',
                    level: 'error'
                });
            }
        })
        .catch((error) => {
            this.setState({
                error
            });

            this.notificationSystem.addNotification({
                message: 'Имя пользователя или Пароль неверны',
                level: 'error'
            });
        });
    }

    cancel(event) {
        event.preventDefault();
        this.context.router.history.push('/')
    }

    onChange(event) {
        const field = event.target.name;
        const credentials = this.state.credentials;
        credentials[field] = event.target.value;

        this.setState({
            credentials
        });

        let errors = this.state.errors;

        const identifier = this.state.credentials.identifier;
        const password = this.state.credentials.password;

        if (identifier === '') {
            errors.identifier = 'Обязательное поле';
        } else {
            errors.identifier = '';
        }

        if (password === '') {
            errors.password = 'Обязательное поле';
        } else {
            errors.password = '';
        }

        if (identifier === '' && password === '') {
            errors = {};
        }

        this.setState({
            errors
        });
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-4 col-md-offset-3">
                    <h2>Аутентификация</h2>
                    <form onSubmit={this.login}>
                        <div className="form-horizontal">

                            <TextField
                                hintText="Имя пользователя"
                                floatingLabelText="Имя пользователя"
                                errorText={this.state.errors.identifier}
                                type="text"
                                name="identifier"
                                onChange={this.onChange}
                                className="input-full-width"
                            />
                            <br/>

                            <TextField
                                hintText="Пароль"
                                floatingLabelText="Пароль"
                                errorText={this.state.errors.password}
                                type="password"
                                name="password"
                                onChange={this.onChange}
                                className="input-full-width"
                            />
                            <br/><br/>
                            <RaisedButton type="submit" label="Войти"  />
                            <FlatButton type="button" label="Отмена" onClick={this.cancel} />

                        </div>
                    </form>
                </div>
                <NotificationSystem ref="notificationSystem" />
            </div>
        );
    }
}

