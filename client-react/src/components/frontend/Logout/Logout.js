import React, {Component} from 'react';
import {Redirect} from 'react-router-dom'

import AuthService from '../../../services/auth';

export default class Logout extends Component {

    static contextTypes = {
        router: React.PropTypes.shape({
            history: React.PropTypes.object.isRequired,
        }),
    };

    constructor(props) {
        super(props);

        AuthService.logout().then(() => {
            this.context.router.history.push('/');
        });

    }

    render() {
        return (
            <Redirect to="/"/>
        )
    }

}

