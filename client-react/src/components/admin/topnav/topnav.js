import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AuthService from '../../../services/auth';

export default class TopNav extends Component {
    render() {
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand">Админпанель</a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav">
                            <li><Link to="/admin">Начало</Link></li>
                            <li><Link to="/admin/users">Пользователи</Link></li>
                            <li><Link to="/admin/groups">Группы</Link></li>
                            <li><Link to="/admin/roles">Роли</Link></li>
                            <li><Link to="/admin/widgets">Виджеты</Link></li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li><a>Привет, {AuthService.getCurrentUser().username}</a></li>
                            <li><Link to="/">На сайт</Link></li>
                            <li><Link to="/auth/logout">Выход</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>



        );
    }
}
