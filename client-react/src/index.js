import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import injectTapEventPlugin from 'react-tap-event-plugin';

import './config'
import './config.local'

import App from './App';

import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import jquery from 'jquery';
window.$ = window.jQuery = global.jQuery = jquery;
require('bootstrap');

injectTapEventPlugin();
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
