import React, {Component} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import LoginPage from './components/frontend/Login/Login'
import Logout from './components/frontend/Logout/Logout'
import HomePage from './components/frontend/Home/Home'
import WidgetsPage from './components/frontend/Widgets/Widgets'

import AdminDashboardPage from './components/admin/Dashboard/Dashboard';
import AdminUsersPage from './components/admin/Users/Users';

import AuthService from './services/auth'

import './App.css';

export default class App extends Component {
    render() {

        return (
            <div className="container">
                <MuiThemeProvider>
                    <BrowserRouter>
                        <Switch>
                            <Route exact path='/' component={HomePage} />

                            <Route path='/auth/login' component={LoginPage} />
                            <Route path='/auth/logout' component={Logout} />

                            <PrivateRoute path='/widgets' component={WidgetsPage} />

                            <Route exact path='/admin' component={AdminDashboardPage} />
                            <Route path='/admin/users' component={AdminUsersPage} />
                        </Switch>
                    </BrowserRouter>
                </MuiThemeProvider>
            </div>
        );
    }
}


const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        AuthService.isAuth() ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{
                pathname: '/auth/login',
                state: { from: props.location }
            }}/>
        )
    )}/>
);