import config from 'react-global-configuration';

let Params = {

    api_host: 'http://localhost:1337',

};

config.set(Params, {
    freeze: false
});

export default Params;