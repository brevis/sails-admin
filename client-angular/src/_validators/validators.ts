import { ReflectiveInjector } from '@angular/core';
import { Http, XHRBackend, ConnectionBackend, BrowserXhr, ResponseOptions, XSRFStrategy, BaseResponseOptions, CookieXSRFStrategy, RequestOptions, BaseRequestOptions } from '@angular/http';
import {AbstractControl, FormControl, ValidatorFn} from '@angular/forms';
import { UsersDataService, PermissionsDataService } from '../_services/index';

export class CustomValidators extends CookieXSRFStrategy {

    private static http: Http = null;
    private static usersDataService: UsersDataService = null;
    private static permissionsDataService: PermissionsDataService = null;

    static uniqueUserUsername(userId: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let value = control.value;
            let service = CustomValidators.getUsersDataService();

            let where = {username: value};
            if (userId !== undefined) {
                where.id = {"!": userId};
            }

            return null; // TODO: research async validators;

            return new Promise((resolve, reject) => {
                service.find(where).subscribe(users => {
                    if (users.length > 0) {
                        resolve({'uniqueUserUsername': true});
                    } else {
                        resolve(null);
                    }
                }, err => resolve(null));
            });

        };
    }

    static uniqueUserEmail(userId: string): ValidatorFn {
        return control => {
            let value = control.value;
            let service = CustomValidators.getUsersDataService();

            let where = {email: value};
            if (userId !== undefined) {
                where.id = {"!": userId};
            }

            return null; // TODO: research async validators;

            return new Promise((resolve, reject) => {
                service.find(where).subscribe(users => {
                    if (users.length > 0) {
                        resolve({'uniqueUserEmail': true});
                    } else {
                        resolve(null);
                    }
                }, err => resolve(null));
            });
        };
    }

    static uniquePermissionName(permissionId: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let value = control.value;
            let service = CustomValidators.getPermissionsDataService();

            let where = {name: value};
            if (permissionId !== undefined) {
                where.id = {"!": permissionId};
            }

            return null; // TODO: research async validators;

            service.find(where).subscribe(users => {
                if (users.length > 0) {
                    return {'uniquePermissionName': true};
                } else {
                    return null;
                }
            }, err => null);
        };
    }

    static getHttp() {
        if (!CustomValidators.http) {
            CustomValidators.http = ReflectiveInjector.resolveAndCreate([
                Http, BrowserXhr,
                {provide: RequestOptions, useClass: BaseRequestOptions},
                {provide: ResponseOptions, useClass: BaseResponseOptions},
                {provide: ConnectionBackend, useClass: XHRBackend},
                {provide: XSRFStrategy, useFactory: () => new CookieXSRFStrategy()}
            ]).get(Http);
        }
        return CustomValidators.http;
    }

    static getUsersDataService() {
        if (!CustomValidators.usersDataService) {
            CustomValidators.usersDataService = new UsersDataService(CustomValidators.getHttp());
        }
        return CustomValidators.usersDataService;
    }

    static getPermissionsDataService() {
        if (!CustomValidators.permissionsDataService) {
            CustomValidators.permissionsDataService = new PermissionsDataService(CustomValidators.getHttp());
        }
        return CustomValidators.permissionsDataService;
    }

}
