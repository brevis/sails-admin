System.register("validators", ['@angular/core', '@angular/http', '../_services/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var core_1, http_1, index_1;
    var CustomValidators;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            CustomValidators = (function (_super) {
                __extends(CustomValidators, _super);
                function CustomValidators() {
                    _super.apply(this, arguments);
                }
                CustomValidators.uniqueUserUsername = function (userId) {
                    return function (control) {
                        var value = control.value;
                        var service = CustomValidators.getUsersDataService();
                        var where = { username: value };
                        if (userId !== undefined) {
                            where.id = { "!": userId };
                        }
                        return null; // TODO: research async validators;
                        return new Promise(function (resolve, reject) {
                            service.find(where).subscribe(function (users) {
                                if (users.length > 0) {
                                    resolve({ 'uniqueUserUsername': true });
                                }
                                else {
                                    resolve(null);
                                }
                            }, function (err) { return resolve(null); });
                        });
                    };
                };
                CustomValidators.uniqueUserEmail = function (userId) {
                    return function (control) {
                        var value = control.value;
                        var service = CustomValidators.getUsersDataService();
                        var where = { email: value };
                        if (userId !== undefined) {
                            where.id = { "!": userId };
                        }
                        return null; // TODO: research async validators;
                        return new Promise(function (resolve, reject) {
                            service.find(where).subscribe(function (users) {
                                if (users.length > 0) {
                                    resolve({ 'uniqueUserEmail': true });
                                }
                                else {
                                    resolve(null);
                                }
                            }, function (err) { return resolve(null); });
                        });
                    };
                };
                CustomValidators.uniquePermissionName = function (permissionId) {
                    return function (control) {
                        var value = control.value;
                        var service = CustomValidators.getPermissionsDataService();
                        var where = { name: value };
                        if (permissionId !== undefined) {
                            where.id = { "!": permissionId };
                        }
                        return null; // TODO: research async validators;
                        service.find(where).subscribe(function (users) {
                            if (users.length > 0) {
                                return { 'uniquePermissionName': true };
                            }
                            else {
                                return null;
                            }
                        }, function (err) { return null; });
                    };
                };
                CustomValidators.getHttp = function () {
                    if (!CustomValidators.http) {
                        CustomValidators.http = core_1.ReflectiveInjector.resolveAndCreate([
                            http_1.Http, http_1.BrowserXhr,
                            { provide: http_1.RequestOptions, useClass: http_1.BaseRequestOptions },
                            { provide: http_1.ResponseOptions, useClass: http_1.BaseResponseOptions },
                            { provide: http_1.ConnectionBackend, useClass: http_1.XHRBackend },
                            { provide: http_1.XSRFStrategy, useFactory: function () { return new http_1.CookieXSRFStrategy(); } }
                        ]).get(http_1.Http);
                    }
                    return CustomValidators.http;
                };
                CustomValidators.getUsersDataService = function () {
                    if (!CustomValidators.usersDataService) {
                        CustomValidators.usersDataService = new index_1.UsersDataService(CustomValidators.getHttp());
                    }
                    return CustomValidators.usersDataService;
                };
                CustomValidators.getPermissionsDataService = function () {
                    if (!CustomValidators.permissionsDataService) {
                        CustomValidators.permissionsDataService = new index_1.PermissionsDataService(CustomValidators.getHttp());
                    }
                    return CustomValidators.permissionsDataService;
                };
                CustomValidators.http = null;
                CustomValidators.usersDataService = null;
                CustomValidators.permissionsDataService = null;
                return CustomValidators;
            }(http_1.CookieXSRFStrategy));
            exports_1("CustomValidators", CustomValidators);
        }
    }
});
