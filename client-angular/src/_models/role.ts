import { User } from './user'

export class Role {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    users: User[];
    reserved: boolean;
}