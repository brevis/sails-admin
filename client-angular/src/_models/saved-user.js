System.register("saved-user", [], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var SavedUser;
    return {
        setters:[],
        execute: function() {
            SavedUser = (function () {
                function SavedUser() {
                    this.isAdmin = false;
                }
                return SavedUser;
            }());
            exports_1("SavedUser", SavedUser);
        }
    }
});
