export class Rawsettings {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    data: string;
}