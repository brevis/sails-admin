import { User } from './user'

export class Permission {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    description: string;
    users: User[];
}