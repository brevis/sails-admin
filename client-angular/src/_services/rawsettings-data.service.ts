import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';
import { Rawsettings } from '../_models/rawsettings';

@Injectable()
export class RawsettingsDataService extends BaseDataService {

    protected modelName: string = 'rawsettings';

    public saveSettings(key: string, data: string) {
        return new Promise((resolve, reject) => {
            this.find({name: key}).subscribe(settings => {

                let model = new Rawsettings();
                model.name = key;
                model.data = data;

                if (settings && settings[0] && settings[0].id) {
                    this.update(settings[0].id, model).subscribe(_ => {
                        resolve(true);
                    }, err => resolve(false) );
                } else {
                    this.add(model).subscribe(_ => {
                        resolve(true);
                    }, err => resolve(false) );
                }
            }, err => resolve(false) )
        });
    }

}