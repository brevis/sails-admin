import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';

@Injectable()
export class RolesDataService extends BaseDataService {

    protected modelName: string = 'role';

}