import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';

@Injectable()
export class UsersDataService extends BaseDataService {

    protected modelName: string = 'user';

    public getRoles(id: string) {
        return this.http.get(BaseDataService.API_HOST + '/api/' + this.modelName + '/' + id + '/roles', this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    public getPermissions(id: string) {
        return this.http.get(BaseDataService.API_HOST + '/api/' + this.modelName + '/' + id + '/permissions', this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    /**
     * Return User object with Roles and Permissions
     *
     * @param id
     * @returns any
     */
    public getUserDetails(id: string) {
        return this.http.get(BaseDataService.API_HOST + '/api/' + this.modelName + '/' + id + '/details', this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

}