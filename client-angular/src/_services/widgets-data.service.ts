import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { BaseDataService } from './base-data.service';
import { Widget } from '../_models/widget';
import { RawsettingsDataService } from './rawsettings-data.service';
import {forEach} from "@angular/router/src/utils/collection";

@Injectable()
export class WidgetsDataService extends BaseDataService {

    public static readonly COLUMNS_COUNT = 3;
    public static readonly SETTINGS_KEY_GROUP_ALL = 'all';
    public static readonly SETTINGS_KEY = 'wigetspositions_{group}';

    protected modelName: string = 'widget';

    constructor(private http: Http,
                private rawsettingsDataService: RawsettingsDataService) {
        super(http);
    }

    /**
     * Get widgets placed by columns
     *
     * @param group
     * @param userId
     * @returns {Promise}
     */
    public getWidgetsPositions(group: string, userId?: string) : Array {
        return new Promise((resolve, reject) => {

            // get widgets for group (or all, if group == SETTINGS_KEY_GROUP_ALL)
            let where = {};
            if (group != WidgetsDataService.SETTINGS_KEY_GROUP_ALL) {
                where.group = group;
            }
            this.find(where).subscribe(widgets => {

                let indexedWidgets = WidgetsDataService.indexObjects(widgets);

                if (!userId) {
                    userId = group;
                }

                // get widgets positions for group
                this.rawsettingsDataService.find({name: WidgetsDataService.getSettingsKey(userId)}).subscribe(widgetsPositions => {

                    try {
                        widgetsPositions = JSON.parse(widgetsPositions[0].data);
                    } catch (e) {
                        widgetsPositions = null;
                    }

                    if (userId && widgetsPositions === null) {
                        resolve(null);
                    }

                    if (!widgetsPositions) {
                        widgetsPositions = [];
                    }

                    let columns = WidgetsDataService.getEmptyColumns(Math.max(widgetsPositions.length, WidgetsDataService.COLUMNS_COUNT));

                    // place widgets to its places
                    widgetsPositions.forEach((positions, column) => {
                        positions.forEach(pos => {
                            if (indexedWidgets[pos.id]) {
                                columns[column].push(indexedWidgets[pos.id]);
                                indexedWidgets[pos.id] = null;
                            }
                        })
                    });

                    // place unplaced widgets to column[0]
                    for (let id in indexedWidgets) {
                        if (indexedWidgets[id]) {
                            columns[0].push(indexedWidgets[id]);
                        }
                    }

                    resolve(columns);
                });
            });
        });
    }

    public saveWidgetsPositions(group: string, columns: Array<any>) {
        columns = columns.map(column => {
            return column.map(widget => {
                return {id: widget.id};
            });
        });
        return this.rawsettingsDataService.saveSettings(WidgetsDataService.getSettingsKey(group), JSON.stringify(columns));
    }

    static getSettingsKey(group: string) : string {
        return WidgetsDataService.SETTINGS_KEY
            .replace('{group}', group);
    }

    static getEmptyColumns(count: number) {
        let columns = [];
        for (let i = 0; i < count; i++) {
            columns.push([]);
        }
        return columns;
    }

    static indexObjects(objects: Array<any>, key: string = 'id') : object {
        let indexedObjects = {};
        objects.forEach(item => {
            indexedObjects[item[key]] = item;
        });
        return indexedObjects;
    }

}