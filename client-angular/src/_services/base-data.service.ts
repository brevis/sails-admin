import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { AuthenticationService } from './authentication.service';

@Injectable()
export abstract class BaseDataService {

    public static readonly API_HOST = 'http://localhost:1337';

    protected abstract modelName: string;

    constructor(private http: Http) {}

    // GET /model
    public getAll() {
        return this.http.get(BaseDataService.API_HOST + '/api/' + this.modelName, this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    // GET /model/:id
    public getOne(id: string) {
        return this.http.get(BaseDataService.API_HOST + '/api/' + this.modelName + '/' + id, this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    // GET /model?where=...
    public find(where: object) {
        return this.http.get(BaseDataService.API_HOST + '/api/' + this.modelName + '?where=' + JSON.stringify(where), this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    // POST /model
    public add(model: any) {
        return this.http.post(BaseDataService.API_HOST + '/api/' + this.modelName, model, this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    // PUT /model/:id
    public update(id: string, values: Object = {}) {
        return this.http.put(BaseDataService.API_HOST + '/api/' + this.modelName + '/' + id, values, this.getAuthorizationHeader()).map((response: Response) => response.json());
    }

    // DELETE /model/:id
    public destroy(id: string) {
        return this.http.delete(BaseDataService.API_HOST + '/api/' + this.modelName + '/' + id, this.getAuthorizationHeader()).map((response: Response) => response.json())
    }

    protected getAuthorizationHeader(headers?: Headers): Headers {
        if (!headers) {
            headers = new Headers();
        }
        headers.append('Authorization', 'Basic ' + AuthenticationService.getToken());
        return headers;
    }

}