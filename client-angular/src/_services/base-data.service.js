System.register("base-data.service", ['@angular/core', '@angular/http', './authentication.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, http_1, authentication_service_1;
    var BaseDataService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (authentication_service_1_1) {
                authentication_service_1 = authentication_service_1_1;
            }],
        execute: function() {
            BaseDataService = (function () {
                function BaseDataService(http) {
                    this.http = http;
                }
                // GET /model
                BaseDataService.prototype.getAll = function () {
                    return this.http.get('/api/' + this.modelName, this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                // GET /model/:id
                BaseDataService.prototype.getOne = function (id) {
                    return this.http.get('/api/' + this.modelName + '/' + id, this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                // GET /model?where=...
                BaseDataService.prototype.find = function (where) {
                    return this.http.get('/api/' + this.modelName + '?where=' + JSON.stringify(where), this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                // POST /model
                BaseDataService.prototype.add = function (model) {
                    return this.http.post('/api/' + this.modelName, model, this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                // PUT /model/:id
                BaseDataService.prototype.update = function (id, values) {
                    if (values === void 0) { values = {}; }
                    return this.http.put('/api/' + this.modelName + '/' + id, values, this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                // DELETE /model/:id
                BaseDataService.prototype.destroy = function (id) {
                    return this.http.delete('/api/' + this.modelName + '/' + id, this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                BaseDataService.prototype.getAuthorizationHeader = function (headers) {
                    if (!headers) {
                        headers = new http_1.Headers();
                    }
                    headers.append('Authorization', 'Basic ' + authentication_service_1.AuthenticationService.getToken());
                    return headers;
                };
                BaseDataService = __decorate([
                    core_1.Injectable()
                ], BaseDataService);
                return BaseDataService;
            }());
            exports_1("BaseDataService", BaseDataService);
        }
    }
});
