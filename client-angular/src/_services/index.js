System.register("index", ['./alert.service', './authentication.service', './users-data.service', './groups-data.service', './roles-data.service', './permissions-data.service', './widgets-data.service', './rawsettings-data.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for(var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters:[
            function (alert_service_1_1) {
                exportStar_1(alert_service_1_1);
            },
            function (authentication_service_1_1) {
                exportStar_1(authentication_service_1_1);
            },
            function (users_data_service_1_1) {
                exportStar_1(users_data_service_1_1);
            },
            function (groups_data_service_1_1) {
                exportStar_1(groups_data_service_1_1);
            },
            function (roles_data_service_1_1) {
                exportStar_1(roles_data_service_1_1);
            },
            function (permissions_data_service_1_1) {
                exportStar_1(permissions_data_service_1_1);
            },
            function (widgets_data_service_1_1) {
                exportStar_1(widgets_data_service_1_1);
            },
            function (rawsettings_data_service_1_1) {
                exportStar_1(rawsettings_data_service_1_1);
            }],
        execute: function() {
        }
    }
});
