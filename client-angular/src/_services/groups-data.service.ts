import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';

@Injectable()
export class GroupsDataService extends BaseDataService {

    protected modelName: string = 'group';

}