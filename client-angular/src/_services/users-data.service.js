System.register("users-data.service", ['@angular/core', './base-data.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, base_data_service_1;
    var UsersDataService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (base_data_service_1_1) {
                base_data_service_1 = base_data_service_1_1;
            }],
        execute: function() {
            UsersDataService = (function (_super) {
                __extends(UsersDataService, _super);
                function UsersDataService() {
                    _super.apply(this, arguments);
                    this.modelName = 'user';
                }
                UsersDataService.prototype.getRoles = function (id) {
                    return this.http.get('/api/' + this.modelName + '/' + id + '/roles', this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                UsersDataService.prototype.getPermissions = function (id) {
                    return this.http.get('/api/' + this.modelName + '/' + id + '/permissions', this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                /**
                 * Return User object with Roles and Permissions
                 *
                 * @param id
                 * @returns any
                 */
                UsersDataService.prototype.getUserDetails = function (id) {
                    return this.http.get('/api/' + this.modelName + '/' + id + '/details', this.getAuthorizationHeader()).map(function (response) { return response.json(); });
                };
                UsersDataService = __decorate([
                    core_1.Injectable()
                ], UsersDataService);
                return UsersDataService;
            }(base_data_service_1.BaseDataService));
            exports_1("UsersDataService", UsersDataService);
        }
    }
});
