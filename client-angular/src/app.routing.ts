import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { WidgetsComponent } from './widgets/index';

import { LoginComponent } from './login/index';
import { LogoutComponent } from './logout/index';
import { AdminDashboardComponent } from './admin/dashboard/index';
import { AdminUsersComponent } from './admin/users/index';
import { AdminUserEditComponent } from './admin/user-edit/index';
import { AdminRolesComponent } from './admin/roles/index';
import { AdminRoleEditComponent } from './admin/role-edit/index';
import { AdminGroupsComponent } from './admin/groups/index';
import { AdminGroupEditComponent } from './admin/group-edit/index';
import { AdminPermissionsComponent } from './admin/permissions/index';
import { AdminPermissionEditComponent } from './admin/permission-edit/index';
import { AdminWidgetsComponent } from './admin/widgets/index';
import { AdminWidgetEditComponent } from './admin/widget-edit/index';
import { AdminWidgetsPositionsComponent } from './admin/widgets-positions/index';

import { AdminAuthGuard, RegisteredAuthGuard } from './_guards/index';

const appRoutes: Routes = [
    // front
    { path: '', component: HomeComponent },
    { path: 'widgets', component: WidgetsComponent, canActivate: [RegisteredAuthGuard] },

    // admin panel
    { path: 'admin', component: AdminDashboardComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/dashboard', component: AdminDashboardComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/users', component: AdminUsersComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/user/edit/:id', component: AdminUserEditComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/groups', component: AdminGroupsComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/group/edit/:id', component: AdminGroupEditComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/roles', component: AdminRolesComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/role/edit/:id', component: AdminRoleEditComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/permissions', component: AdminPermissionsComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/permission/edit/:id', component: AdminPermissionEditComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/widgets', component: AdminWidgetsComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/widget/edit/:id', component: AdminWidgetEditComponent, canActivate: [AdminAuthGuard] },
    { path: 'admin/widgets/positions', component: AdminWidgetsPositionsComponent, canActivate: [AdminAuthGuard] },

    // auth
    { path: 'auth/login', name: 'login', component: LoginComponent },
    { path: 'auth/logout', name: 'logout', component: LogoutComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
