import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { BaseAuthGuard } from '../_guards/base-auth.guard';
import { SavedUser } from "../_models/saved-user";

import './frontend-nav.component.css';

@Component({
    moduleId: module.id.toString(),
    selector: 'frontend-nav',
    templateUrl: 'frontend-nav.component.html',
    //styleUrls: ['frontend-nav.component.css'],
})

export class FrontendNavComponent implements OnInit {

    protected user: SavedUser = null;

    constructor (private authService: AuthenticationService,
                 private ref:ChangeDetectorRef) {

    }

    ngOnInit() {
        this.authService.getCurrentUser().then(currentUser => {
            this.user = currentUser;
            if (!this.user || !this.user.token) {
                this.user = null;
            }
        });
    }

    public isAdminLogged(): boolean {
        return this.user && this.user.isAdmin;
    }

}