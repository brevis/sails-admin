System.register("index", ['./alert.component', './admin-nav.component', './frontend-nav.component', './modal.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for(var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters:[
            function (alert_component_1_1) {
                exportStar_1(alert_component_1_1);
            },
            function (admin_nav_component_1_1) {
                exportStar_1(admin_nav_component_1_1);
            },
            function (frontend_nav_component_1_1) {
                exportStar_1(frontend_nav_component_1_1);
            },
            function (modal_component_1_1) {
                exportStar_1(modal_component_1_1);
            }],
        execute: function() {
        }
    }
});
