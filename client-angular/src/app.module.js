System.register("app.module", ['@angular/core', '@angular/platform-browser', '@angular/forms', '@angular/http', './app.component', './app.routing', './_directives/index', './_guards/index', './_services/index', './home/index', './widgets/index', './login/index', './logout/index', './admin/dashboard/index', './admin/users/index', './admin/user-edit/index', './admin/groups/index', './admin/group-edit/index', './admin/roles/index', './admin/role-edit/index', './admin/permissions/index', './admin/permission-edit/index', './admin/widgets/index', './admin/widget-edit/index', './admin/widgets-positions/index', '@angular/platform-browser/animations', '@angular/material', 'ng2-dnd'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, platform_browser_1, forms_1, http_1, forms_2, app_component_1, app_routing_1, index_1, index_2, index_3, index_4, index_5, index_6, index_7, index_8, index_9, index_10, index_11, index_12, index_13, index_14, index_15, index_16, index_17, index_18, index_19, index_20, index_21, index_22, index_23, index_24, index_25, animations_1, material_1, ng2_dnd_1;
    var AppModule;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
                forms_2 = forms_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (app_routing_1_1) {
                app_routing_1 = app_routing_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (index_2_1) {
                index_2 = index_2_1;
            },
            function (index_3_1) {
                index_3 = index_3_1;
                index_11 = index_3_1;
                index_14 = index_3_1;
                index_17 = index_3_1;
                index_20 = index_3_1;
                index_24 = index_3_1;
                index_25 = index_3_1;
            },
            function (index_4_1) {
                index_4 = index_4_1;
            },
            function (index_5_1) {
                index_5 = index_5_1;
            },
            function (index_6_1) {
                index_6 = index_6_1;
            },
            function (index_7_1) {
                index_7 = index_7_1;
            },
            function (index_8_1) {
                index_8 = index_8_1;
            },
            function (index_9_1) {
                index_9 = index_9_1;
            },
            function (index_10_1) {
                index_10 = index_10_1;
            },
            function (index_12_1) {
                index_12 = index_12_1;
            },
            function (index_13_1) {
                index_13 = index_13_1;
            },
            function (index_15_1) {
                index_15 = index_15_1;
            },
            function (index_16_1) {
                index_16 = index_16_1;
            },
            function (index_18_1) {
                index_18 = index_18_1;
            },
            function (index_19_1) {
                index_19 = index_19_1;
            },
            function (index_21_1) {
                index_21 = index_21_1;
            },
            function (index_22_1) {
                index_22 = index_22_1;
            },
            function (index_23_1) {
                index_23 = index_23_1;
            },
            function (animations_1_1) {
                animations_1 = animations_1_1;
            },
            function (material_1_1) {
                material_1 = material_1_1;
            },
            function (ng2_dnd_1_1) {
                ng2_dnd_1 = ng2_dnd_1_1;
            }],
        execute: function() {
            AppModule = (function () {
                function AppModule() {
                }
                AppModule = __decorate([
                    core_1.NgModule({
                        imports: [
                            platform_browser_1.BrowserModule,
                            forms_1.FormsModule,
                            http_1.HttpModule,
                            app_routing_1.routing,
                            forms_1.FormsModule,
                            forms_2.ReactiveFormsModule,
                            animations_1.BrowserAnimationsModule,
                            material_1.MdButtonModule, material_1.MdToolbarModule, material_1.MdInputModule, material_1.MdSelectModule, material_1.MdGridListModule,
                            ng2_dnd_1.DndModule.forRoot(),
                        ],
                        declarations: [
                            app_component_1.AppComponent,
                            index_1.AlertComponent,
                            index_1.AdminNavComponent,
                            index_1.FrontendNavComponent,
                            index_1.ModalComponent,
                            index_4.HomeComponent,
                            index_5.WidgetsComponent,
                            index_6.LoginComponent,
                            index_7.LogoutComponent,
                            //RegisterComponent,
                            index_8.AdminDashboardComponent,
                            index_9.AdminUsersComponent,
                            index_10.AdminUserEditComponent,
                            index_12.AdminGroupsComponent,
                            index_13.AdminGroupEditComponent,
                            index_15.AdminRolesComponent,
                            index_16.AdminRoleEditComponent,
                            index_18.AdminPermissionsComponent,
                            index_19.AdminPermissionEditComponent,
                            index_21.AdminWidgetsComponent,
                            index_22.AdminWidgetEditComponent,
                            index_23.AdminWidgetsPositionsComponent,
                        ],
                        providers: [
                            index_2.AdminAuthGuard,
                            index_2.RegisteredAuthGuard,
                            index_3.AlertService,
                            index_3.AuthenticationService,
                            index_11.UsersDataService,
                            index_14.GroupsDataService,
                            index_17.RolesDataService,
                            index_20.PermissionsDataService,
                            index_24.WidgetsDataService,
                            index_25.RawsettingsDataService,
                        ],
                        bootstrap: [app_component_1.AppComponent]
                    })
                ], AppModule);
                return AppModule;
            }());
            exports_1("AppModule", AppModule);
        }
    }
});
