System.register("home.component", ['@angular/core', '../_services/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, index_1;
    var HomeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            HomeComponent = (function () {
                function HomeComponent(widgetsDataService, authService, ref) {
                    this.widgetsDataService = widgetsDataService;
                    this.authService = authService;
                    this.ref = ref;
                }
                HomeComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.authService.getCurrentUser().then(function (user) {
                        _this.currentUser = user;
                        if (_this.currentUser) {
                            _this.widgetsDataService.getWidgetsPositions(index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL, _this.currentUser.id).then(function (widgets) {
                                if (widgets && widgets.length) {
                                    _this.widgetsColumns = widgets;
                                }
                                else {
                                    _this.widgetsDataService.getWidgetsPositions(index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL).then(function (widgets) {
                                        _this.widgetsColumns = widgets;
                                    });
                                }
                            });
                        }
                    });
                    // dirty hack to refresh page
                    // TODO: research wtf;
                    setInterval(function () {
                        _this.currentUser = _this.currentUser;
                    }, 10);
                };
                HomeComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'home.component.html',
                        styleUrls: ['home.component.css']
                    })
                ], HomeComponent);
                return HomeComponent;
            }());
            exports_1("HomeComponent", HomeComponent);
        }
    }
});
