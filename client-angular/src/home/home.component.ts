import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { User, SavedUser } from '../_models/index';
import { AuthenticationService, WidgetsDataService } from '../_services/index';

import './home.component.css';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'home.component.html',
    //styleUrls: ['home.component.css'],
})

export class HomeComponent implements OnInit {

    protected currentUser: SavedUser;

    protected widgetsColumns: Array<any>;

    constructor(private widgetsDataService: WidgetsDataService,
                private authService: AuthenticationService,
                private ref:ChangeDetectorRef) {

    }

    ngOnInit() {
        this.authService.getCurrentUser().then(user => {
            this.currentUser = user;
            if (this.currentUser) {
                this.widgetsDataService.getWidgetsPositions(WidgetsDataService.SETTINGS_KEY_GROUP_ALL, this.currentUser.id).then(widgets => {
                    if (widgets && widgets.length) {
                        this.widgetsColumns = widgets;
                    } else {
                        this.widgetsDataService.getWidgetsPositions(WidgetsDataService.SETTINGS_KEY_GROUP_ALL).then(widgets => {
                            this.widgetsColumns = widgets;
                        });
                    }
                });
            }
        });

        // dirty hack to refresh page
        // TODO: research wtf;
        setInterval( () => {
            this.currentUser = this.currentUser;
        }, 10);
    }

}