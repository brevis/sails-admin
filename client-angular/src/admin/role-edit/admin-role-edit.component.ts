import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { Role } from '../../_models/role';
import { RolesDataService } from '../../_services/roles-data.service';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-role-edit.component.html'
})

export class AdminRoleEditComponent implements OnInit {

    protected roleId: string;
    protected role: Role = null;
    protected roleForm: FormGroup = new FormGroup({
        name: new FormControl(''),
    });

    protected formErrors = {
        name: '',
    };

    protected validationMessages = {
        name: {
            required: 'Обязательно.',
            maxlength: 'Не может быть больше 30 символов.',
        },
    };

    constructor(
        private dataService: RolesDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.loadRole();
    }

    loadRole() {
        this.roleId = this.route.snapshot.params.id;
        if (this.roleId === '0') {
            this.role = new Role();
            this.buildForm();
        } else {
            this.dataService.getOne(this.roleId).subscribe(role => {
                this.role = role;
                this.buildForm();
            }, err => {
                this.alertService.error(`Роль с id "${this.roleId}" не найдена.`);
            });
        }
    }

    buildForm() {
        this.roleForm = new FormGroup({
            name: new FormControl(this.role.name, [Validators.required, Validators.maxLength(30)]),
        });

        this.roleForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.roleForm) {
            return;
        }
        const form = this.roleForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    saveRole() {
        const formData = this.roleForm.value;
        if (this.roleId === '0') {
            this.dataService.add(formData).subscribe(role => {
                this.alertService.success('Роль добавлена');
                this.role = new Role();
                this.buildForm();
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        } else {
            this.dataService.update(this.roleId, formData).subscribe(role => {
                this.alertService.success('Роль сохранена');
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        }
    }

    cancel() {
        this.router.navigate(['/admin/roles']);
    }

}
