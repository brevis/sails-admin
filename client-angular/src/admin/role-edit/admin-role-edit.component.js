System.register("admin-role-edit.component", ['@angular/core', '@angular/forms', '../../_models/role'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, forms_1, role_1;
    var AdminRoleEditComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (role_1_1) {
                role_1 = role_1_1;
            }],
        execute: function() {
            AdminRoleEditComponent = (function () {
                function AdminRoleEditComponent(dataService, location, router, route, alertService) {
                    this.dataService = dataService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.role = null;
                    this.roleForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl('')
                    });
                    this.formErrors = {
                        name: ''
                    };
                    this.validationMessages = {
                        name: {
                            required: 'Required.',
                            maxlength: 'Cannot be more than 30 characters long.'
                        }
                    };
                }
                AdminRoleEditComponent.prototype.ngOnInit = function () {
                    this.loadRole();
                };
                AdminRoleEditComponent.prototype.loadRole = function () {
                    var _this = this;
                    this.roleId = this.route.snapshot.params.id;
                    if (this.roleId === '0') {
                        this.role = new role_1.Role();
                        this.buildForm();
                    }
                    else {
                        this.dataService.getOne(this.roleId).subscribe(function (role) {
                            _this.role = role;
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error("Role with id \"" + _this.roleId + "\" not found.");
                        });
                    }
                };
                AdminRoleEditComponent.prototype.buildForm = function () {
                    var _this = this;
                    this.roleForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl(this.role.name, [forms_1.Validators.required, forms_1.Validators.maxLength(30)])
                    });
                    this.roleForm.valueChanges
                        .subscribe(function (data) { return _this.onValueChanged(data); });
                    this.onValueChanged();
                };
                AdminRoleEditComponent.prototype.onValueChanged = function (data) {
                    if (!this.roleForm) {
                        return;
                    }
                    var form = this.roleForm;
                    for (var field in this.formErrors) {
                        // clear previous error message (if any)
                        this.formErrors[field] = '';
                        var control = form.get(field);
                        if (control && control.dirty && !control.valid) {
                            var messages = this.validationMessages[field];
                            for (var key in control.errors) {
                                this.formErrors[field] += messages[key] + ' ';
                            }
                        }
                    }
                };
                AdminRoleEditComponent.prototype.saveRole = function () {
                    var _this = this;
                    var formData = this.roleForm.value;
                    if (this.roleId === '0') {
                        this.dataService.add(formData).subscribe(function (role) {
                            _this.alertService.success('Role was added');
                            _this.role = new role_1.Role();
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                    else {
                        this.dataService.update(this.roleId, formData).subscribe(function (role) {
                            _this.alertService.success('Role was updated');
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                };
                AdminRoleEditComponent.prototype.cancel = function () {
                    this.router.navigate(['/admin/roles']);
                };
                AdminRoleEditComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-role-edit.component.html'
                    })
                ], AdminRoleEditComponent);
                return AdminRoleEditComponent;
            }());
            exports_1("AdminRoleEditComponent", AdminRoleEditComponent);
        }
    }
});
