import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../_services/index';
import { User } from '../../_models/user';
import { Widget } from '../../_models/widget';
import { WidgetsDataService } from '../../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-widgets.component.html'
})

export class AdminWidgetsComponent {

    protected widgets: Widget[];

    protected currentWidget: Widget = null;

    constructor(
        private dataService: WidgetsDataService,
        private alertService: AlertService) {
        this.loadWidgets();
    }

    deleteWidget(widget) {
        let widgetId = widget.id;
        this.dataService.destroy(widget.id).subscribe(_widget => {
            if (_widget && widgetId === _widget.id) {
                this.loadWidgets();
            } else {
                this.alertService.error('Виджет с id "' + widgetId + '" не найден');
            }

            this.currentWidget = null;
        });
    }

    loadWidgets() {
        this.dataService.getAll().subscribe(widgets => { this.widgets = widgets; });
    }
}
