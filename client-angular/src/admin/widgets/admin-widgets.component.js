System.register("admin-widgets.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var AdminWidgetsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AdminWidgetsComponent = (function () {
                function AdminWidgetsComponent(dataService, alertService) {
                    this.dataService = dataService;
                    this.alertService = alertService;
                    this.currentWidget = null;
                    this.loadWidgets();
                }
                AdminWidgetsComponent.prototype.deleteWidget = function (widget) {
                    var _this = this;
                    var widgetId = widget.id;
                    this.dataService.destroy(widget.id).subscribe(function (_widget) {
                        if (_widget && widgetId === _widget.id) {
                            _this.loadWidgets();
                        }
                        else {
                            _this.alertService.error('Widget with id "' + widgetId + '" not found');
                        }
                        _this.currentWidget = null;
                    });
                };
                AdminWidgetsComponent.prototype.loadWidgets = function () {
                    var _this = this;
                    this.dataService.getAll().subscribe(function (widgets) { _this.widgets = widgets; });
                };
                AdminWidgetsComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-widgets.component.html'
                    })
                ], AdminWidgetsComponent);
                return AdminWidgetsComponent;
            }());
            exports_1("AdminWidgetsComponent", AdminWidgetsComponent);
        }
    }
});
