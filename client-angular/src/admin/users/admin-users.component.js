System.register("admin-users.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var AdminUsersComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AdminUsersComponent = (function () {
                function AdminUsersComponent(dataService, alertService) {
                    this.dataService = dataService;
                    this.alertService = alertService;
                    this.currentUser = null;
                    this.loadUsers();
                }
                AdminUsersComponent.prototype.deleteUser = function (user) {
                    var _this = this;
                    var userId = user.id;
                    this.dataService.destroy(user.id).subscribe(function (_user) {
                        if (_user && userId === _user.id) {
                            _this.loadUsers();
                        }
                        else {
                            _this.alertService.error('User with id "' + userId + '" not found');
                        }
                        _this.currentUser = null;
                    });
                };
                AdminUsersComponent.prototype.loadUsers = function () {
                    var _this = this;
                    this.dataService.getAll().subscribe(function (users) { _this.users = users; });
                };
                AdminUsersComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-users.component.html'
                    })
                ], AdminUsersComponent);
                return AdminUsersComponent;
            }());
            exports_1("AdminUsersComponent", AdminUsersComponent);
        }
    }
});
