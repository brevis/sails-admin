import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../_services/index';
import { User } from '../../_models/user';
import { UsersDataService } from '../../_services/users-data.service';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-users.component.html'
})

export class AdminUsersComponent {

    protected users: User[];
    protected currentUser: User = null;

    constructor(
        private dataService: UsersDataService,
        private alertService: AlertService) {
        this.loadUsers();
    }

    deleteUser(user) {
        let userId = user.id;
        this.dataService.destroy(user.id).subscribe(_user => {
            if (_user && userId === _user.id) {
                this.loadUsers();
            } else {
                this.alertService.error('Пользователь с id "' + userId + '" не найден');
            }

            this.currentUser = null;
        });
    }

    loadUsers() {
        this.dataService.getAll().subscribe(users => { this.users = users; });
    }
}
