import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { Widget, Group } from '../../_models/index';
import { WidgetsDataService, GroupsDataService } from '../../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-widget-edit.component.html'
})

export class AdminWidgetEditComponent implements OnInit {

    protected widgetId: string;

    protected widget: Widget = null;

    protected groups: Group[] = [];

    protected sizes: Array<string> = [];

    protected widgetForm: FormGroup = new FormGroup({
        name: new FormControl(''),
        code: new FormControl(''),
        enabled: new FormControl(''),
        groups: new FormControl(''),
        minWidth: new FormControl(''),
        minHeight: new FormControl(''),
        maxWidth: new FormControl(''),
        maxHeight: new FormControl(''),
    });

    protected formErrors = {
        name: '',
        code: '',
        enabled: '',
        groups: '',
        size: '',
    };

    protected validationMessages = {
        name: {
            required: 'Обязательно.',
            maxlength: 'Не может быть больше 30 символов.',
        },
        code: {
            maxlength: 'Не может быть больше 10000 символов.',
        },
        minWidth: {
            required: 'Обязательно.',
        },
        minHeight: {
            required: 'Обязательно.',
        },
        maxWidth: {
            required: 'Обязательно.',
        },
        maxHeight: {
            required: 'Обязательно.',
        },
    };

    constructor(
        private dataService: WidgetsDataService,
        private groupsDataService: GroupsDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.loadWidget();

        // groups
        this.groupsDataService.getAll().subscribe(groups => {
            this.groups = groups;
        });

        // sizes
        for (let w = 2; w <= 10; w++) {
            for (let h = 2; h <= 10; h++) {
                this.sizes.push(w + 'x' + h);
            }
        }
    }

    loadWidget() {
        this.widgetId = this.route.snapshot.params.id;
        if (this.widgetId === '0') {
            this.widget = new Widget();
            this.buildForm();
        } else {
            this.dataService.getOne(this.widgetId).subscribe(widget => {
                this.widget = widget;
                this.buildForm();
            }, err => {
                this.alertService.error(`Виджет с id "${this.widgetId}" не найден.`);
            });
        }
    }

    buildForm() {
        this.widgetForm = new FormGroup({
            name: new FormControl(this.widget.name, [Validators.required, Validators.maxLength(30)]),
            code: new FormControl(this.widget.code, [Validators.maxLength(10000)]),
            enabled: new FormControl(this.widget.enabled),
            groups: new FormControl(this.widget.groups),
            minWidth: new FormControl(this.widget.minWidth),
            minHeight: new FormControl(this.widget.minHeight),
            maxWidth: new FormControl(this.widget.maxWidth),
            maxHeight: new FormControl(this.widget.maxHeight),
        });

        this.widgetForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    onValueChanged(data?: any) {
        if (!this.widgetForm) {
            return;
        }
        const form = this.widgetForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    saveWidget() {
        const formData = this.widgetForm.value;
        if (this.widgetId === '0') {
            this.dataService.add(formData).subscribe(widget => {
                this.alertService.success('Виджет добавлен');
                this.widget = new Widget();
                this.buildForm();
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        } else {
            this.dataService.update(this.widgetId, formData).subscribe(widget => {
                this.alertService.success('Виджет сохранен');
            }, err => {
                this.alertService.error(err);
                err = JSON.parse(err);
                console.log(err);
            });
        }
    }

    cancel() {
        this.router.navigate(['/admin/widgets']);
    }

}
