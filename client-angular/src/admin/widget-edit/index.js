System.register("index", ['./admin-widget-edit.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for(var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters:[
            function (admin_widget_edit_component_1_1) {
                exportStar_1(admin_widget_edit_component_1_1);
            }],
        execute: function() {
        }
    }
});
