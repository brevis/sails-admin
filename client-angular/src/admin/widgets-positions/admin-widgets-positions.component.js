System.register("admin-widgets-positions.component", ['@angular/core', '../../_services/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, index_1;
    var AdminWidgetsPositionsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            AdminWidgetsPositionsComponent = (function () {
                function AdminWidgetsPositionsComponent(dataService, groupsDataService, widgetsDataService, location, router, route, alertService) {
                    this.dataService = dataService;
                    this.groupsDataService = groupsDataService;
                    this.widgetsDataService = widgetsDataService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.widgets = [];
                    this.groups = [];
                    this.groupAll = index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL;
                    this.columns = [];
                }
                AdminWidgetsPositionsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.groupsDataService.getAll().subscribe(function (groups) {
                        _this.groups = groups;
                    });
                    this.loadWidgetPositions();
                };
                AdminWidgetsPositionsComponent.prototype.loadWidgetPositions = function (group) {
                    var _this = this;
                    if (!group) {
                        group = index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL;
                    }
                    this.widgetsDataService.getWidgetsPositions(group).then(function (columns) {
                        _this.columns = columns;
                    });
                };
                AdminWidgetsPositionsComponent.prototype.saveWidgetsOrder = function (group) {
                    var _this = this;
                    if (!group) {
                        group = index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL;
                    }
                    this.widgetsDataService.saveWidgetsPositions(group, this.columns).then(function (status) {
                        if (status) {
                            _this.alertService.success('Widgets positions was saved');
                        }
                        else {
                            _this.alertService.error('Something went wrong. Please try again.');
                        }
                    });
                };
                AdminWidgetsPositionsComponent.prototype.cancel = function () {
                    this.router.navigate(['/admin/widgets']);
                };
                AdminWidgetsPositionsComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-widgets-positions.component.html',
                        styleUrls: ['admin-widgets-positions.component.css', '/node_modules/ng2-dnd/style.css']
                    })
                ], AdminWidgetsPositionsComponent);
                return AdminWidgetsPositionsComponent;
            }());
            exports_1("AdminWidgetsPositionsComponent", AdminWidgetsPositionsComponent);
        }
    }
});
