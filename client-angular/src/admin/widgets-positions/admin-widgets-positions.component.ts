import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../../_services/index';
import { Widget, Group, Rawsettings } from '../../_models/index';
import { WidgetsDataService, GroupsDataService, RawsettingsDataService } from '../../_services/index';
import { DndModule } from 'ng2-dnd';

import './admin-widgets-positions.component.css';
import '../../../node_modules/ng2-dnd/style.css';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-widgets-positions.component.html',
    //styleUrls: ['admin-widgets-positions.component.css'/*, '../../..//node_modules/ng2-dnd/style.css'*/],
})

export class AdminWidgetsPositionsComponent implements OnInit {

    protected widgets: Widget[] = [];

    protected groups: Group[] = [];

    protected groupAll: string = WidgetsDataService.SETTINGS_KEY_GROUP_ALL;

    protected columns: Array<Array<Widget>> = [];

    constructor(
        private dataService: WidgetsDataService,
        private groupsDataService: GroupsDataService,
        private widgetsDataService: WidgetsDataService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.groupsDataService.getAll().subscribe(groups => {
            this.groups = groups;
        });

        this.loadWidgetPositions();
    }

    public loadWidgetPositions(group?: string) {
        if (!group) {
            group = WidgetsDataService.SETTINGS_KEY_GROUP_ALL;
        }
        this.widgetsDataService.getWidgetsPositions(group).then(columns => {
            this.columns = columns;
        });
    }

    public saveWidgetsOrder(group?: string) {
        if (!group) {
            group = WidgetsDataService.SETTINGS_KEY_GROUP_ALL;
        }
        this.widgetsDataService.saveWidgetsPositions(group, this.columns).then(status => {
            if (status) {
                this.alertService.success('Расположение виджетов было сохранено');
            } else {
                this.alertService.error('Что-то пошло не так. Пожалуйста, попробуйте еще раз позже.');
            }
        });
    }

    public cancel() {
        this.router.navigate(['/admin/widgets']);
    }

}
