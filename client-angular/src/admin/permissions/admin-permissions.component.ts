import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../_services/index';
import { User } from '../../_models/user';
import { Permission } from '../../_models/permission';
import { PermissionsDataService } from '../../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-permissions.component.html'
})

export class AdminPermissionsComponent {

    protected permissions: Permission[];

    protected currentPermission: Permission = null;

    constructor(
        private dataService: PermissionsDataService,
        private alertService: AlertService) {
        this.loadPermissions();
    }

    deletePermission(permission) {
        let permissionId = permission.id;
        this.dataService.destroy(permission.id).subscribe(_permission => {
            if (_permission && permissionId === _permission.id) {
                this.loadPermissions();
            } else {
                this.alertService.error('Запись с id "' + permissionId + '" не найдена');
            }

            this.currentPermission = null;
        });
    }

    loadPermissions() {
        this.dataService.getAll().subscribe(permissions => { this.permissions = permissions; });
    }
}
