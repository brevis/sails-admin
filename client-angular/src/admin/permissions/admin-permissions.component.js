System.register("admin-permissions.component", ['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1;
    var AdminPermissionsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            AdminPermissionsComponent = (function () {
                function AdminPermissionsComponent(dataService, alertService) {
                    this.dataService = dataService;
                    this.alertService = alertService;
                    this.currentPermission = null;
                    this.loadPermissions();
                }
                AdminPermissionsComponent.prototype.deletePermission = function (permission) {
                    var _this = this;
                    var permissionId = permission.id;
                    this.dataService.destroy(permission.id).subscribe(function (_permission) {
                        if (_permission && permissionId === _permission.id) {
                            _this.loadPermissions();
                        }
                        else {
                            _this.alertService.error('Permission with id "' + permissionId + '" not found');
                        }
                        _this.currentPermission = null;
                    });
                };
                AdminPermissionsComponent.prototype.loadPermissions = function () {
                    var _this = this;
                    this.dataService.getAll().subscribe(function (permissions) { _this.permissions = permissions; });
                };
                AdminPermissionsComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-permissions.component.html'
                    })
                ], AdminPermissionsComponent);
                return AdminPermissionsComponent;
            }());
            exports_1("AdminPermissionsComponent", AdminPermissionsComponent);
        }
    }
});
