System.register("admin-group-edit.component", ['@angular/core', '@angular/forms', '../../_models/group'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, forms_1, group_1;
    var AdminGroupEditComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (group_1_1) {
                group_1 = group_1_1;
            }],
        execute: function() {
            AdminGroupEditComponent = (function () {
                function AdminGroupEditComponent(dataService, location, router, route, alertService) {
                    this.dataService = dataService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.group = null;
                    this.groupForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl('')
                    });
                    this.formErrors = {
                        name: ''
                    };
                    this.validationMessages = {
                        name: {
                            required: 'Required.',
                            maxlength: 'Cannot be more than 30 characters long.'
                        }
                    };
                }
                AdminGroupEditComponent.prototype.ngOnInit = function () {
                    this.loadGroup();
                };
                AdminGroupEditComponent.prototype.loadGroup = function () {
                    var _this = this;
                    this.groupId = this.route.snapshot.params.id;
                    if (this.groupId === '0') {
                        this.group = new group_1.Group();
                        this.buildForm();
                    }
                    else {
                        this.dataService.getOne(this.groupId).subscribe(function (group) {
                            _this.group = group;
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error("Group with id \"" + _this.groupId + "\" not found.");
                        });
                    }
                };
                AdminGroupEditComponent.prototype.buildForm = function () {
                    var _this = this;
                    this.groupForm = new forms_1.FormGroup({
                        name: new forms_1.FormControl(this.group.name, [forms_1.Validators.required, forms_1.Validators.maxLength(30)])
                    });
                    this.groupForm.valueChanges
                        .subscribe(function (data) { return _this.onValueChanged(data); });
                    this.onValueChanged();
                };
                AdminGroupEditComponent.prototype.onValueChanged = function (data) {
                    if (!this.groupForm) {
                        return;
                    }
                    var form = this.groupForm;
                    for (var field in this.formErrors) {
                        // clear previous error message (if any)
                        this.formErrors[field] = '';
                        var control = form.get(field);
                        if (control && control.dirty && !control.valid) {
                            var messages = this.validationMessages[field];
                            for (var key in control.errors) {
                                this.formErrors[field] += messages[key] + ' ';
                            }
                        }
                    }
                };
                AdminGroupEditComponent.prototype.saveGroup = function () {
                    var _this = this;
                    var formData = this.groupForm.value;
                    if (this.groupId === '0') {
                        this.dataService.add(formData).subscribe(function (group) {
                            _this.alertService.success('Group was added');
                            _this.group = new group_1.Group();
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                    else {
                        this.dataService.update(this.groupId, formData).subscribe(function (group) {
                            _this.alertService.success('Group was updated');
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                };
                AdminGroupEditComponent.prototype.cancel = function () {
                    this.router.navigate(['/admin/groups']);
                };
                AdminGroupEditComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-group-edit.component.html'
                    })
                ], AdminGroupEditComponent);
                return AdminGroupEditComponent;
            }());
            exports_1("AdminGroupEditComponent", AdminGroupEditComponent);
        }
    }
});
