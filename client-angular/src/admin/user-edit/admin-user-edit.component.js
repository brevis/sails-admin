System.register("admin-user-edit.component", ['@angular/core', '@angular/forms', '../../_models/index', '../../_validators/validators'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, forms_1, index_1, validators_1;
    var AdminUserEditComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (validators_1_1) {
                validators_1 = validators_1_1;
            }],
        execute: function() {
            AdminUserEditComponent = (function () {
                function AdminUserEditComponent(usersDataService, rolesDataService, groupsDataService, location, router, route, alertService, formBuilder) {
                    this.usersDataService = usersDataService;
                    this.rolesDataService = rolesDataService;
                    this.groupsDataService = groupsDataService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.formBuilder = formBuilder;
                    this.user = null;
                    this.userRolesIds = [];
                    this.userPermissionsIds = [];
                    this.groups = [];
                    this.roles = [];
                    this.formErrors = {
                        username: '',
                        email: '',
                        password: '',
                        roles: '',
                        group: ''
                    };
                    this.validationMessages = {
                        username: {
                            required: 'Required.',
                            minlength: 'Must be at least 3 characters long.',
                            maxlength: 'Cannot be more than 30 characters long.',
                            uniqueUserUsername: 'This Username already taken.'
                        },
                        email: {
                            required: 'Required.',
                            email: 'Incorrect.',
                            uniqueUserEmail: 'This Email already taken.'
                        },
                        password: {
                            minlength: 'Must be at least 6 characters long.'
                        },
                        roles: {
                            required: 'Required.'
                        }
                    };
                }
                AdminUserEditComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.loadUser();
                    this.rolesDataService.getAll().subscribe(function (roles) {
                        _this.roles = roles;
                    });
                    this.groupsDataService.getAll().subscribe(function (groups) {
                        _this.groups = groups;
                    });
                };
                AdminUserEditComponent.prototype.loadUser = function () {
                    var _this = this;
                    this.userId = this.route.snapshot.params.id;
                    if (this.userId === '0') {
                        this.user = new index_1.User();
                        this.buildForm();
                    }
                    else {
                        this.usersDataService.getUserDetails(this.userId).subscribe(function (user) {
                            _this.user = user;
                            user.roles.forEach(function (role) {
                                _this.userRolesIds.push(role.id);
                            });
                            user.permissions.forEach(function (permission) {
                                _this.userPermissionsIds.push(permission.id);
                            });
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error("User with id \"" + _this.userId + "\" not found.");
                        });
                    }
                };
                AdminUserEditComponent.prototype.buildForm = function () {
                    var _this = this;
                    var passwordsValidators = [forms_1.Validators.minLength(6)];
                    if (this.user.id === '0') {
                        passwordsValidators.push(forms_1.Validators.required);
                    }
                    this.userForm = this.formBuilder.group({
                        username: new forms_1.FormControl(this.user.username, [forms_1.Validators.required, forms_1.Validators.minLength(3), forms_1.Validators.maxLength(30), validators_1.CustomValidators.uniqueUserUsername(this.user.id)]),
                        email: new forms_1.FormControl(this.user.email, [forms_1.Validators.required, forms_1.Validators.email, validators_1.CustomValidators.uniqueUserEmail(this.user.id)]),
                        password: new forms_1.FormControl('', passwordsValidators),
                        roles: new forms_1.FormControl(this.userRolesIds, [forms_1.Validators.required]),
                        group: new forms_1.FormControl(this.user.group)
                    });
                    this.userForm.valueChanges
                        .subscribe(function (data) { return _this.onValueChanged(data); });
                    this.onValueChanged();
                };
                AdminUserEditComponent.prototype.onValueChanged = function (data) {
                    if (!this.userForm) {
                        return;
                    }
                    var form = this.userForm;
                    for (var field in this.formErrors) {
                        // clear previous error message (if any)
                        this.formErrors[field] = '';
                        var control = form.get(field);
                        if (control && control.dirty && !control.valid) {
                            var messages = this.validationMessages[field];
                            for (var key in control.errors) {
                                this.formErrors[field] += messages[key] + ' ';
                            }
                        }
                    }
                };
                AdminUserEditComponent.prototype.saveUser = function () {
                    var _this = this;
                    var formData = this.userForm.value;
                    if (this.userId === '0') {
                        this.usersDataService.add(formData).subscribe(function (user) {
                            _this.alertService.success('User was added');
                            _this.user = new index_1.User();
                            _this.buildForm();
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                    else {
                        this.usersDataService.update(this.userId, formData).subscribe(function (user) {
                            _this.alertService.success('User was updated');
                        }, function (err) {
                            _this.alertService.error(err);
                            err = JSON.parse(err);
                            console.log(err);
                        });
                    }
                };
                AdminUserEditComponent.prototype.cancel = function () {
                    this.router.navigate(['/admin/users']);
                };
                AdminUserEditComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'admin-user-edit.component.html',
                        styleUrls: ['admin-user-edit.component.css']
                    })
                ], AdminUserEditComponent);
                return AdminUserEditComponent;
            }());
            exports_1("AdminUserEditComponent", AdminUserEditComponent);
        }
    }
});
