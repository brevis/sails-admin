import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../_services/index';
import { User } from '../../_models/user';
import { Group } from '../../_models/group';
import { GroupsDataService } from '../../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'admin-groups.component.html'
})

export class AdminGroupsComponent {

    protected groups: Group[];

    protected currentGroup: Group = null;

    constructor(
        private dataService: GroupsDataService,
        private alertService: AlertService) {
        this.loadGroups();
    }

    deleteGroup(group) {
        let groupId = group.id;
        this.dataService.destroy(group.id).subscribe(_group => {
            if (_group && groupId === _group.id) {
                this.loadGroups();
            } else {
                this.alertService.error('Группа с id "' + groupId + '" не найдена');
            }

            this.currentGroup = null;
        });
    }

    loadGroups() {
        this.dataService.getAll().subscribe(groups => { this.groups = groups; });
    }
}
