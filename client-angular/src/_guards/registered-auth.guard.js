System.register("registered-auth.guard", ['@angular/core', './base-auth.guard', '../_services/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, base_auth_guard_1, index_1;
    var RegisteredAuthGuard;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (base_auth_guard_1_1) {
                base_auth_guard_1 = base_auth_guard_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            RegisteredAuthGuard = (function (_super) {
                __extends(RegisteredAuthGuard, _super);
                function RegisteredAuthGuard() {
                    _super.apply(this, arguments);
                    this.redirectUrl = ['/'];
                    this.redirectMessage = 'Access denied';
                    this.roles = [index_1.AuthenticationService.ROLE_REGISTERED, index_1.AuthenticationService.ROLE_ADMIN];
                    this.rolesEvery = false;
                }
                RegisteredAuthGuard = __decorate([
                    core_1.Injectable()
                ], RegisteredAuthGuard);
                return RegisteredAuthGuard;
            }(base_auth_guard_1.BaseAuthGuard));
            exports_1("RegisteredAuthGuard", RegisteredAuthGuard);
        }
    }
});
