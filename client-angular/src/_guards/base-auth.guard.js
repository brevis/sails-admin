System.register("base-auth.guard", ['@angular/core', '../_services/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, index_1;
    var BaseAuthGuard;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            BaseAuthGuard = (function () {
                function BaseAuthGuard(router, authService, alertService) {
                    this.router = router;
                    this.authService = authService;
                    this.alertService = alertService;
                    this.redirectUrl = ['/auth/login'];
                    this.redirectMessage = 'Access denied';
                    this.roles = [index_1.AuthenticationService.ROLE_ADMIN];
                    this.rolesEvery = false;
                }
                BaseAuthGuard.prototype.canActivate = function (route, state) {
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        return _this.authService.getCurrentUser().then(function (currentUser) {
                            if (index_1.AuthenticationService.hasRoles(currentUser, _this.roles, _this.rolesEvery)) {
                                resolve(true);
                            }
                            else {
                                _this.navigate(_this.redirectUrl, state.url, _this.redirectMessage);
                                resolve(false);
                            }
                        }).catch(function (err) {
                            _this.navigate(_this.redirectUrl, state.url, _this.redirectMessage);
                            resolve(false);
                        });
                    });
                };
                BaseAuthGuard.prototype.navigate = function (urls, returnUrl, message) {
                    var queryParams = {};
                    if (returnUrl) {
                        queryParams = {
                            queryParams: {
                                returnUrl: returnUrl
                            }
                        };
                    }
                    this.router.navigate(urls, queryParams);
                    if (message) {
                        this.alertService.error(message);
                    }
                };
                BaseAuthGuard = __decorate([
                    core_1.Injectable()
                ], BaseAuthGuard);
                return BaseAuthGuard;
            }());
            exports_1("BaseAuthGuard", BaseAuthGuard);
        }
    }
});
