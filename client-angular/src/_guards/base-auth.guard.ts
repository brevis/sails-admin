import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService, AlertService } from '../_services/index';

@Injectable()
export abstract class BaseAuthGuard implements CanActivate {

    protected abstract redirectUrl = ['/auth/login'];
    protected abstract redirectMessage = 'Access denied';
    protected abstract roles = [AuthenticationService.ROLE_ADMIN];
    protected abstract rolesEvery = false;

    constructor(protected router: Router,
                protected authService: AuthenticationService,
                protected alertService: AlertService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return new Promise((resolve, reject) => {
            return this.authService.getCurrentUser().then(currentUser => {
                if (AuthenticationService.hasRoles(currentUser, this.roles, this.rolesEvery)) {
                    resolve(true);
                } else {
                    this.navigate(this.redirectUrl, state.url, this.redirectMessage);
                    resolve(false);
                }
            }).catch(err => {
                this.navigate(this.redirectUrl, state.url, this.redirectMessage);
                resolve(false);
            });
        });
    }

    public navigate(urls: any, returnUrl?: string, message?: string) {
        let queryParams = {};
        if (returnUrl) {
            queryParams = {
                queryParams: {
                    returnUrl: returnUrl
                }
            };
        }
        this.router.navigate(urls, queryParams);
        if (message) {
            this.alertService.error(message);
        }
    }
}