import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlertService } from '../_services/index';
import { Widget, Group, Rawsettings } from '../_models/index';
import { WidgetsDataService, GroupsDataService, RawsettingsDataService, AuthenticationService } from '../_services/index';
import { DndModule } from 'ng2-dnd';

import './widgets.component.css';
import '../../node_modules/ng2-dnd/style.css';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'widgets.component.html',
    //styleUrls: ['widgets.component.css'/*, '../../node_modules/ng2-dnd/style.css'*/],
})

export class WidgetsComponent implements OnInit {

    protected currentUser: object;

    protected groupAll: string = WidgetsDataService.SETTINGS_KEY_GROUP_ALL;

    protected columns: Array<Array<Widget>> = [];

    constructor(
        private dataService: WidgetsDataService,
        private groupsDataService: GroupsDataService,
        private widgetsDataService: WidgetsDataService,
        private authService: AuthenticationService,
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService) {

    }

    ngOnInit() {
        this.currentUser = AuthenticationService.getSavedUser();
        this.loadWidgetPositions();
    }

    public loadWidgetPositions() {
        this.widgetsDataService.getWidgetsPositions(WidgetsDataService.SETTINGS_KEY_GROUP_ALL, this.currentUser.id).then(columns => {
            if (columns && columns.length) {
                this.columns = columns;
            } else {
                this.widgetsDataService.getWidgetsPositions(WidgetsDataService.SETTINGS_KEY_GROUP_ALL).then(columns => {
                    this.columns = columns;
                });
            }
        });
    }

    public saveWidgetsOrder(group?: string) {
        this.widgetsDataService.saveWidgetsPositions(this.currentUser.id, this.columns).then(status => {
            if (status) {
                this.alertService.success('Расположение виджетов было сохранено');
            } else {
                this.alertService.error('Что-то пошло не так. Пожалйста, попробуйте еще раз позже.');
            }
        });
    }

    public cancel() {
        this.router.navigate(['/']);
    }

}
