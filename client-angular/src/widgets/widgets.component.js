System.register("widgets.component", ['@angular/core', '../_services/index'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, index_1;
    var WidgetsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }],
        execute: function() {
            WidgetsComponent = (function () {
                function WidgetsComponent(dataService, groupsDataService, widgetsDataService, authService, location, router, route, alertService) {
                    this.dataService = dataService;
                    this.groupsDataService = groupsDataService;
                    this.widgetsDataService = widgetsDataService;
                    this.authService = authService;
                    this.location = location;
                    this.router = router;
                    this.route = route;
                    this.alertService = alertService;
                    this.groupAll = index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL;
                    this.columns = [];
                }
                WidgetsComponent.prototype.ngOnInit = function () {
                    this.currentUser = index_1.AuthenticationService.getSavedUser();
                    this.loadWidgetPositions();
                };
                WidgetsComponent.prototype.loadWidgetPositions = function () {
                    var _this = this;
                    this.widgetsDataService.getWidgetsPositions(index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL, this.currentUser.id).then(function (columns) {
                        if (columns && columns.length) {
                            _this.columns = columns;
                        }
                        else {
                            _this.widgetsDataService.getWidgetsPositions(index_1.WidgetsDataService.SETTINGS_KEY_GROUP_ALL).then(function (columns) {
                                _this.columns = columns;
                            });
                        }
                    });
                };
                WidgetsComponent.prototype.saveWidgetsOrder = function (group) {
                    var _this = this;
                    this.widgetsDataService.saveWidgetsPositions(this.currentUser.id, this.columns).then(function (status) {
                        if (status) {
                            _this.alertService.success('Widgets positions was saved');
                        }
                        else {
                            _this.alertService.error('Something went wrong. Please try again.');
                        }
                    });
                };
                WidgetsComponent.prototype.cancel = function () {
                    this.router.navigate(['/']);
                };
                WidgetsComponent = __decorate([
                    core_1.Component({
                        moduleId: module.id,
                        templateUrl: 'widgets.component.html',
                        styleUrls: ['widgets.component.css', '/node_modules/ng2-dnd/style.css']
                    })
                ], WidgetsComponent);
                return WidgetsComponent;
            }());
            exports_1("WidgetsComponent", WidgetsComponent);
        }
    }
});
