import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent, AdminNavComponent, FrontendNavComponent, ModalComponent } from './_directives/index';
import { AdminAuthGuard, RegisteredAuthGuard } from './_guards/index';
import { AlertService, AuthenticationService } from './_services/index';

import { HomeComponent } from './home/index';
import { WidgetsComponent } from './widgets/index';

import { LoginComponent } from './login/index';
import { LogoutComponent } from './logout/index';

import { AdminDashboardComponent } from './admin/dashboard/index';

import { AdminUsersComponent } from './admin/users/index';
import { AdminUserEditComponent } from './admin/user-edit/index';
import { UsersDataService } from './_services/index';

import { AdminGroupsComponent } from './admin/groups/index';
import { AdminGroupEditComponent } from './admin/group-edit/index';
import { GroupsDataService } from './_services/index';

import { AdminRolesComponent } from './admin/roles/index';
import { AdminRoleEditComponent } from './admin/role-edit/index';
import { RolesDataService } from './_services/index';

import { AdminPermissionsComponent } from './admin/permissions/index';
import { AdminPermissionEditComponent } from './admin/permission-edit/index';
import { PermissionsDataService } from './_services/index';

import { AdminWidgetsComponent } from './admin/widgets/index';
import { AdminWidgetEditComponent } from './admin/widget-edit/index';
import { AdminWidgetsPositionsComponent } from './admin/widgets-positions/index';
import { WidgetsDataService } from './_services/index';

import { RawsettingsDataService } from './_services/index';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdButtonModule, MdToolbarModule, MdInputModule, MdSelectModule, MdGridListModule } from '@angular/material';

import { DndModule } from 'ng2-dnd';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        FormsModule,
        ReactiveFormsModule,

        BrowserAnimationsModule,
        MdButtonModule, MdToolbarModule, MdInputModule, MdSelectModule, MdGridListModule,

        DndModule.forRoot(),
    ],
    declarations: [
        AppComponent,

        AlertComponent,
        AdminNavComponent,
        FrontendNavComponent,
        ModalComponent,

        HomeComponent,
        WidgetsComponent,
        LoginComponent,
        LogoutComponent,
        //RegisterComponent,
        AdminDashboardComponent,
        AdminUsersComponent,
        AdminUserEditComponent,
        AdminGroupsComponent,
        AdminGroupEditComponent,
        AdminRolesComponent,
        AdminRoleEditComponent,
        AdminPermissionsComponent,
        AdminPermissionEditComponent,
        AdminWidgetsComponent,
        AdminWidgetEditComponent,
        AdminWidgetsPositionsComponent,
    ],
    providers: [
        AdminAuthGuard,
        RegisteredAuthGuard,
        AlertService,
        AuthenticationService,
        UsersDataService,
        GroupsDataService,
        RolesDataService,
        PermissionsDataService,
        WidgetsDataService,
        RawsettingsDataService,
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }