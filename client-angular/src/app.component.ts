import { Component } from '@angular/core';

import '../node_modules/@angular/material/prebuilt-themes/indigo-pink.css';

@Component({
    moduleId: module.id.toString(),
    selector: 'app',
    templateUrl: 'app.component.html'
})

export class AppComponent { }