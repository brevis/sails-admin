import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { MdButtonModule, MdInputModule } from '@angular/material';

import { AlertService, AuthenticationService } from '../_services/index';

import './login.component.css';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'login.component.html',
    //styleUrls: ['login.component.css'],
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private http: Http) {

    }

    ngOnInit() {
        this.authenticationService.logout();
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        let currentUser = this.authenticationService.getCurrentUser();
        if (currentUser.id) {
            this.router.navigate([this.returnUrl]);
        }
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                    this.loading = false;
                },
                error => {
                    let errorMsg = error;
                    if (error.status == 403) {
                        errorMsg = 'Имя пользователя или Пароль неверны';
                    }
                    this.alertService.error(errorMsg);
                    this.loading = false;
                });
    }
}
